
resource "aws_lex_bot" "industry_x_ref_app_partneronboarding_Bot" {
  depends_on=[aws_lambda_permission.bot_lex]
  abort_statement {
    message {
      content_type = "PlainText"
      content      = "Sorry, I am not able to assist at this time"
    }
  }

  child_directed = false

  clarification_prompt {
    max_attempts = 2

    message {
      content_type = "PlainText"
      content      = "Sorry, what can I help you with?"
    }
  }

  description                 = "Bot to update and add Partner Details"
  detect_sentiment            = false
  idle_session_ttl_in_seconds = 600

 #update status
   intent {
     intent_name    = aws_lex_intent.update_Status.name
     intent_version = aws_lex_intent.update_Status.version
   } 

 #update address
  intent {
     intent_name    = aws_lex_intent.update_Address.name
    intent_version = aws_lex_intent.update_Address.version
   }
 #continue
   intent {
     intent_name    = aws_lex_intent.Continue_updating.name
     intent_version = aws_lex_intent.Continue_updating.version
   }
 #intro
   intent {
     intent_name    = aws_lex_intent.Introduce_Partner.name
     intent_version = aws_lex_intent.Introduce_Partner.version
  }
 #moreOptions
   intent {
     intent_name    = aws_lex_intent.more_Options.name
     intent_version = aws_lex_intent.more_Options.version
   }
 #SignUP
   intent {
     intent_name    = aws_lex_intent.SignUp_Intro.name
     intent_version = aws_lex_intent.SignUp_Intro.version
   }
 #session end
   intent {
     intent_name    = aws_lex_intent.Session_End.name
     intent_version = aws_lex_intent.Session_End.version
   }
 #updateAverage
  intent {
    intent_name    = aws_lex_intent.update_Average.name
    intent_version = aws_lex_intent.update_Average.version
  }
 #updateCategory
   intent {
     intent_name    = aws_lex_intent.update_Category.name
     intent_version = aws_lex_intent.update_Category.version
  }
 #updateCuisine
   intent {
     intent_name    = aws_lex_intent.update_Cuisine.name
     intent_version = aws_lex_intent.update_Cuisine.version
   }
 #updateimage_url

   intent {
     intent_name    = aws_lex_intent.updateimage_url.name
     intent_version = aws_lex_intent.updateimage_url.version
   }
  
 #updatelocation
   intent {
     intent_name    = aws_lex_intent.update_location.name
     intent_version = aws_lex_intent.update_location.version
   }
    intent {
     intent_name    = aws_lex_intent.update_Name.name
     intent_version = aws_lex_intent.update_Name.version
   }
  
   intent {
     intent_name    = aws_lex_intent.updateoperation_timings.name
     intent_version = aws_lex_intent.updateoperation_timings.version
   } 
   intent {
     intent_name    = aws_lex_intent.add_Partner.name
     intent_version = aws_lex_intent.add_Partner.version
  }
  
#name

  locale           = "en-US"
  name             = "industry_x_ref_app_partneronboarding_Bot" #var.lex_bot_name
  process_behavior = "BUILD"
  voice_id         = "Salli"
 }

