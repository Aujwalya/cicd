locals {
  bucket_name = "onboardings11"
}
# output "endpoint"{
#   value=aws_s3_bucket.main.endpoint
# }
output "Bucket_name"{
  value=aws_s3_bucket.main.bucket
}
resource "aws_s3_bucket" "main" {
  bucket = local.bucket_name
  acl    = "private"
  policy = data.aws_iam_policy_document.bucket_policy.json

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  force_destroy ="false"

  tags = {
    "Name" = local.bucket_name
  }
}

data "aws_iam_policy_document" "bucket_policy" {
  statement {
    sid = "AllowReadFromAll"

    actions = [
      "s3:GetObject",
    ]

    resources = [
      "arn:aws:s3:::${local.bucket_name}/*",
    ]

    principals {
      type        = "*"
      identifiers = ["*"]
    }
  }
}

# Refactor it to use loop
resource "aws_s3_bucket_object" "index" {
  bucket       = local.bucket_name
  key          = "index.html"
  source       = "../modules/acc_aws_lex/partner/index.html"
  content_type = "text/html"
}


