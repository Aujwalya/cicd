# resource "aws_iam_role_policy" "frontend_lambda_role_policys" {
#   name   = "frontend-lambda-role-policyss"
#   role   = "${aws_iam_role.lambda_exec2.id}"
#   policy = file("../modules/acc_aws_lex/s3_update.json")
# }
resource "aws_lambda_function" "lex_update" {
  function_name = "lex_update"
  filename         = "../modules/acc_aws_lex/partner/update_lex.zip"
  handler = "update_lex.lambda_handler"
  runtime = "python3.8"
  timeout = 10
  role = aws_iam_role.lambda_exec2.arn
  layers = [ aws_lambda_layer_version.lambda_layer_soup.arn ]
  environment {
    variables = {
      bucket_name  = aws_cloudformation_stack.lex_deploy.outputs.WebAppBucket
      file_name = "index.html"
    }
}
}
# resource "aws_lambda_layer_version" "lambda_layer_soup" {
#   layer_name = "Beautiful_soup_module"
#   filename = "../modules/acc_aws_lex/Soup.zip"
#   compatible_runtimes = ["python3.8","python3.7","python3.6"]
# }

resource "aws_iam_role" "lambda_exec2" {
   name = "serverless_partner-handlers"
 
   assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["lambda.amazonaws.com"]
      },
      "Effect": "Allow",
      "Sid": ""
      
    }
  ]
}
EOF
 
}



data "aws_lambda_invocation" "lex_update" {
  function_name = aws_lambda_function.lex_update.function_name

  input = <<JSON
{
  "key1": "value1",
  "key2": "value2"
}

JSON
}
