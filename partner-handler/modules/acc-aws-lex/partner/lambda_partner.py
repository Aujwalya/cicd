import json
import boto3
import os
from bs4 import BeautifulSoup

s3=boto3.client("s3")
s31=boto3.resource("s3")
bucket_name= os.environ.get('bucket_name')
s3_file_name=os.environ.get('file_name')
url_chatbot="\""+os.environ.get('url_chatbot')+"\""
obj = s31.Object(bucket_name,s3_file_name)

def lambda_handler(event, context):
    body = obj.get()['Body'].read()
    body = body.decode('UTF-8')
    soup = BeautifulSoup(body,'html.parser')
    script = soup.find("div",{"id": "url_chatbot"})
    script.text
    a=script.text
    
    body=body.replace(a,url_chatbot)
    body = bytes(body, 'utf-8')
    s3.put_object(Bucket=bucket_name,Key=s3_file_name,Body=body,ContentType='text/html')
        
    return body