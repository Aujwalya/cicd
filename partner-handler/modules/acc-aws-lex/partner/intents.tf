# #done
#category wale m response card all slot
 data "aws_caller_identity" "current"{}
 data "aws_region" "current"{}
resource "aws_lex_intent" "updateimage_url" {
  
  fulfillment_activity {

  type="CodeHook"
  
  code_hook {
    message_version = "1.0"
    uri="${aws_lambda_function.Lex_bot.arn}"
    }
  }
  name = "updateimage_url"
  sample_utterances = [

    "update image"

  ]

  slot {

    description = "To update average"

    name        = "urls"

    priority    = 1
    slot_constraint = "Required"
    slot_type       = "AMAZON.TIME"
    #slot_type_version = aws_lex_slot_type.urls.version
  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Please enter the URL that shows the image :"

        content_type = "PlainText"

      }

    }

   

  }



}
#done
resource "aws_lex_intent" "update_location" {

 depends_on=[aws_lambda_permission.bot_lex]

 fulfillment_activity {

  type="CodeHook"
  code_hook {
    message_version = "1.0"
    uri="${aws_lambda_function.Lex_bot.arn}"
    }
 
  }
  name = "update_location"

  sample_utterances = [

    "modify location","change location","update location","update my location"

  ]

 slot {

    description = "To update loaction"
    name        = "location"
    priority    = 1
    slot_constraint = "Required"
    slot_type       = "AMAZON.BroadcastChannel"

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Please enter the updated location !"

        content_type = "PlainText"

      }

    }

   

  }

 


 }
#done

resource "aws_lex_intent" "update_Name" {
  depends_on=[aws_lambda_permission.bot_lex]
 fulfillment_activity {

  type="CodeHook"
  code_hook {
    message_version = "1.0"
    uri="${aws_lambda_function.Lex_bot.arn}"
    }
 }
 

  name = "update_Name"

  sample_utterances = [

    "update name"

  ]

 slot {

    description = "To update name"

    name        = "name"

    priority    = 1

   

    slot_constraint = "Required"

    slot_type       = "AMAZON.FictionalCharacter"

    #slot_type_version = aws_lex_slot_type.category.version

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Please enter the updated name !"

        content_type = "PlainText"

      }

    }

   

  }

 

 }
#done
resource "aws_lex_intent" "updateoperation_timings" {

 depends_on=[aws_lambda_permission.bot_lex]

  fulfillment_activity {

    type = "ReturnIntent"

  }

 

  name = "updateoperation_timings"

 

  sample_utterances = [

    "update time","update operating time","update times",

  ]

 slot {

    description = "To update start_time"

    name        = "start_time"

    priority    = 1

   

    slot_constraint = "Required"

    slot_type       = "AMAZON.TIME"

    #slot_type_version = aws_lex_slot_type.category.version

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Please provide the start time in HH:MM format :"

        content_type = "PlainText"

      }

    }

   

  }

 slot {

    description = "To update end_time"

    name        = "end_time"

    priority    = 2

   

    slot_constraint = "Required"

    slot_type       = "AMAZON.TIME"

    #slot_type_version = aws_lex_slot_type.category.version

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Please provide the end time in HH:MM format :"

        content_type = "PlainText"

      }

    }

   

  }

 }





resource "aws_lex_intent" "update_Average" {
  depends_on=[aws_lambda_permission.bot_lex]
 fulfillment_activity {

  type="CodeHook"

  code_hook {

    message_version = "1.0"

    uri="${aws_lambda_function.Lex_bot.arn}"

  }
 }
 

  name = "update_Average"

  sample_utterances = [

    "update average"

  ]

 slot {

    description = "To update average"

    name        = "average"

    priority    = 1

   

    slot_constraint = "Required"

    slot_type       = "AMAZON.NUMBER"

    #slot_type_version = aws_lex_slot_type.category.version

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Please enter the updated Average Cost !"

        content_type = "PlainText"

      }

    }

   

  }

 }

resource "aws_lex_intent" "update_Category" {
 depends_on=[aws_lambda_permission.bot_lex]
 fulfillment_activity {

  type="CodeHook"

 code_hook {
    message_version = "1.0"
    uri="${aws_lambda_function.Lex_bot.arn}"
    }
  }
 
  name = "update_Category"

  sample_utterances = [

    "update category"

  ]

 slot {

    description = "To update category"

    name        = "category"

    priority    = 1

   

    slot_constraint = "Required"

    slot_type       = aws_lex_slot_type.category.name

    slot_type_version = aws_lex_slot_type.category.version

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Please select the intended category to be updated! (Delivery for Delivery, Pick Up for Pick Up , Both for Delivery/Pick Up)"

        content_type = "PlainText"

      }

    }

   

  }

 

 }

resource "aws_lex_intent" "SignUp_Intro" {

 

  fulfillment_activity {

    type = "ReturnIntent"

  }

 

  name = "SignUp_Intro"

  sample_utterances = [

    "Good Morning"

  ]

 

 }

resource "aws_lex_intent" "Session_End" {
  depends_on=[aws_lambda_permission.bot_lex]
  fulfillment_activity {

  type="CodeHook"

 code_hook {
    message_version = "1.0"
    uri="${aws_lambda_function.Lex_bot.arn}"
    }

  }
 

  name = "SessionEnd"

  sample_utterances = [

    "Exit","no","GoodBye","Bye.","Thanks.","Thank You"

  ]

  }


resource "aws_lex_intent" "more_Options" {
   depends_on=[aws_lambda_permission.bot_lex]
  fulfillment_activity {

  type="CodeHook"

  code_hook {
    message_version = "1.0"
    uri="${aws_lambda_function.Lex_bot.arn}"
    }
 }

 

  name = "more_Options"

  sample_utterances = [

    "more options"

  ]

 }

resource "aws_lex_intent" "update_Status" {
  depends_on=[aws_lambda_permission.bot_lex]
 fulfillment_activity {

  type="CodeHook"

  code_hook {
    message_version = "1.0"
    uri="${aws_lambda_function.Lex_bot.arn}"
    }
 }

 

  name = "update_Status"

  sample_utterances = [

    "update status"

  ]

 

 

 slot {

    description = "To update status"

    name        = "status"

    priority    = 1

   

    slot_constraint = "Required"
  


    slot_type       = aws_lex_slot_type.status.name

    slot_type_version = aws_lex_slot_type.status.version
  

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Please select the status you would like to change ! (true for active $ false for inactive)"

        content_type = "PlainText"

      }

    }

   

  }

 }

resource "aws_lex_intent" "update_Address" {
 fulfillment_activity {

  type="CodeHook"
 code_hook {
    message_version = "1.0"
    uri="${aws_lambda_function.Lex_bot.arn}"
    }
  
 }
 

  name = "update_Address"

  sample_utterances = [

    "update address"

  ]

 

  slot {

    description = "To update Address"

    name        = "updatAddress"

    priority    = 1

 

    slot_constraint = "Required"

    slot_type       = "AMAZON.BroadcastChannel"

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Please enter the address to be updated !"

        content_type = "PlainText"

      }

    }

   

  }
 }
 

resource "aws_lex_intent" "Continue_updating" {
   fulfillment_activity {

  type="CodeHook"

  code_hook {
    message_version = "1.0"
    uri="${aws_lambda_function.Lex_bot.arn}"
    }
 }

 

  name = "Continue_updating"

  sample_utterances = [

    "continue"

  ]

 

 }

resource "aws_lex_intent" "Introduce_Partner" {
  depends_on=[aws_lambda_permission.bot_lex]
 fulfillment_activity {

  type="CodeHook"
  code_hook {
    message_version = "1.0"
    uri="${aws_lambda_function.Lex_bot.arn}"
    }
 }

 

  name = "Introduce_Partner"

  sample_utterances = [

   "hello","Hi"

  ]

 }


 

 

 

 

 

 

 

resource "aws_lex_intent" "add_Partner" {

  depends_on=[aws_lambda_permission.bot_lex]

 fulfillment_activity {

  type="CodeHook"

  code_hook {
    message_version = "1.0"
    uri="${aws_lambda_function.Lex_bot.arn}"
    }
 }

  name = "add_Partner"

  sample_utterances = [

    "register"

  ]

 #1

 slot {

    description = "To add name"

    name        = "addname"

    priority    = 1

   

    slot_constraint = "Required"

   slot_type       = "AMAZON.AlphaNumeric"

    #slot_type_version = aws_lex_slot_type.category.version

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Please Enter You Restaurant Name!"

        content_type = "PlainText"

      }

    } 

  }

  #2

 slot {

    description = "To add category"

    name        = "addcategory"

    priority    = 2

   

    slot_constraint = "Required"

    slot_type       = "AMAZON.NUMBER"

    #slot_type_version = aws_lex_slot_type.category.version

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Your Category Please! Delivery for Delivery, Pick Up for Pick Up , Both for Delivery/Pick Up"

        content_type = "PlainText"

      }

    }

   

  }

 #3

  slot {

    description = "To add cusine"

    name        = "addcuisine"

    priority    = 3

   

    slot_constraint = "Required"

    slot_type       = "AMAZON.Food"

    #slot_type_version = aws_lex_slot_type.category.version

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Please Enter Your Cuisine!"

        content_type = "PlainText"

      }

    }

   

  }

 #4

 slot {

    description = "To add cost"

    name        = "addaverageCost"

    priority    = 4

   

    slot_constraint = "Required"

    slot_type       = "AMAZON.NUMBER"

    #slot_type_version = aws_lex_slot_type.category.version

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Please Enter Cost for 2 Person!"

        content_type = "PlainText"

      }

    }

   

  }

 #5

 slot {

    description = "To add location"

    name        = "addlocation"

    priority    = 5

   

    slot_constraint = "Required"

    slot_type       = "AMAZON.AlphaNumeric"

    #slot_type_version = aws_lex_slot_type.category.version

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Where is your Restaurant Located!"

        content_type = "PlainText"

      }

    }

   

  }

 #6

 slot {

    description = "To add address"

    name        = "addaddress"

    priority    = 6

   

    slot_constraint = "Required"

    slot_type       = "AMAZON.BroadcastChannel"

    #slot_type_version = aws_lex_slot_type.category.version

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Please Enter Your Address!"

        content_type = "PlainText"

      }

    }

   

  }

  #7

 slot {

    description = "To add status"

    name        = "addisActive"

    priority    = 7

   

    slot_constraint = "Required"

    slot_type       = "status"

    slot_type_version = aws_lex_slot_type.status.version

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Restaurant Active/Inactive(true for active and false for inactive)"

        content_type = "PlainText"

      }

    }

   

  }

 #8

 

 slot {

    description = "To add url"

    name        = "addimgUrl"

    priority    = 8

   

    slot_constraint = "Required"

    slot_type       = aws_lex_slot_type.urls.name

    slot_type_version = aws_lex_slot_type.urls.version

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Your Restaurant Logo!"

        content_type = "PlainText"

      }

    }

   

  }

 #9

 slot {

    description = "To add start time"

    name        = "addstart_time"

    priority    = 9

   

    slot_constraint = "Required"

    slot_type       = "AMAZON.TIME"

    #slot_type_version = aws_lex_slot_type.urls.version

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "When Your Restaurant Opening Time(HH:MM Am/Pm)!"

        content_type = "PlainText"

      }

    }

   

  }

 

  #10

 

  slot {

    description = "To add end time"

    name        = "addend_time"

    priority    = 10

   

    slot_constraint = "Required"

    slot_type       = "AMAZON.TIME"

    #slot_type_version = aws_lex_slot_type.urls.version

  value_elicitation_prompt {

      max_attempts = 2

 

      message {

        content      = "Restaurant Closing Time(HH:MM Am/Pm)!"

        content_type = "PlainText"

      }

    }

   

  }

 

 }
resource "aws_lex_intent" "update_Cuisine" {

 fulfillment_activity {
  type="CodeHook"
  code_hook {
    message_version = "1.0"
    uri="${aws_lambda_function.Lex_bot.arn}"
    }
  
  #expected fulfillment_activity.0.type to be one of [ReturnIntent CodeHook], got 
  }
  name = "update_Cuisine"
  sample_utterances = [
    "update cuisine"
  ]
 slot {
    description = "To update average"
    name        = "food"
    priority    = 1
    
    slot_constraint = "Required"
    slot_type       = "AMAZON.Food"
    #slot_type_version = aws_lex_slot_type.category.version
  value_elicitation_prompt {
      max_attempts = 2

      message {
        content      = "Please enter the updated cuisine !"
        content_type = "PlainText"
      }
    }
    
  }

 }





