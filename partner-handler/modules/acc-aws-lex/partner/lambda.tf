resource "aws_iam_role_policy" "frontend_lambda_role_policys" {
  name   = "frontend-lambda-role-policys"
  role   = "${aws_iam_role.lambda_exec.id}"
  policy = file("../modules/acc_aws_lex/pol.json")
}

resource "aws_lambda_function" "Lex_bot" {
   
  function_name = "Lex_bot"
  filename      = "../modules/acc_aws_lex/lex_code/Lambda_Lex_Code.zip"
  role = aws_iam_role.lambda_exec.arn
  handler = "Lambda_Lex_Code.lambda_handler"
  runtime = "python3.8"
  timeout = 10
  layers = [ aws_lambda_layer_version.lambda_layer.arn ]
}
resource "aws_lambda_layer_version" "lambda_layer" {
  layer_name = "Request_Module"
  filename = "../modules/acc_aws_lex/lex_code/Requests.zip"
  compatible_runtimes = ["python3.8","python3.7","python3.6"]
}



resource "aws_lambda_permission" "bot_lex" {
   
  #statement_id  = "AllowExecutionFromLex"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.Lex_bot.function_name
  principal     = "lex.amazonaws.com"
}



resource "aws_lambda_alias" "test_alias" {
  name             = "testalias"
  description      = "a sample description"
  function_name    = aws_lambda_function.Lex_bot.function_name
  function_version = "$LATEST"
}


resource "aws_iam_role" "lambda_exec" {
   name = "serverless_partner-handler_lambda"
 
   assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["lambda.amazonaws.com","lex.amazonaws.com"]
      },
      "Effect": "Allow",
      "Sid": ""
      
    }
  ]
}
EOF
 
}

