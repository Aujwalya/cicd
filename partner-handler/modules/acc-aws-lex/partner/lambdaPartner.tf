resource "aws_iam_role_policy" "frontend_lambda_role_policy" {
  name   = "frontend-lambda-role-policys"
  role   = "${aws_iam_role.lambda_exec1.id}"
  policy = file("../modules/acc_aws_lex/s3_update.json")
}

# data "archive_file" "first_zip" {
#   type        = "zip"
#   source_file = "lambda_partner.py"
#   output_path = "lambda_partner.zip"
# }
resource "aws_lambda_function" "url_chatbot" {
  depends_on = [aws_s3_bucket.main]
  function_name = "url_chatbot"
  filename         = "../modules/acc_aws_lex/partner/lambda_partner.zip"
  #source_code_hash = data.archive_file.first_zip.output_base64sha256
  handler = "lambda_partner.lambda_handler"
  runtime = "python3.8"
  timeout = 10
  role = aws_iam_role.lambda_exec1.arn
  layers = [ aws_lambda_layer_version.lambda_layer_soup.arn ]
  environment {
    variables = {
      bucket_name  = local.bucket_name
      file_name = "index.html"
      url_chatbot = aws_cloudformation_stack.lex_deploy.outputs.WebAppUrl
    }
}
}

resource "aws_lambda_layer_version" "lambda_layer_soup" {
  layer_name = "Beautiful_soup_module"
  filename = "../modules/acc_aws_lex/Soup.zip"
  compatible_runtimes = ["python3.8","python3.7","python3.6"]
}

resource "aws_iam_role" "lambda_exec1" {
   name = "serverless_partner-handler"
   assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["lambda.amazonaws.com"]
      },
      "Effect": "Allow",
      "Sid": ""
      
    }
  ]
}
EOF
 
}


