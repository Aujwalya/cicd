
output "lambda_arn" {
  description = "The Amazon Resource Name (ARN) identifying your Lambda Function."
  value       = aws_lambda_function.Lex_bot.arn
}

output "cloud_Front_url_output" {
  description = "The Amazon Resource Name (ARN) identifying your Lambda Function."
  value       = aws_cloudformation_stack.lex_deploy.outputs.WebAppUrl
}

output "cloud_Front_webbucket_output" {
  value       = aws_cloudformation_stack.lex_deploy.outputs.WebAppBucket
}

# output "cloud_url" {
#   description = "cloudFront"
#    value       = aws_cloudfront_distribution.s3_distribution.domain_name
# }
