
resource "aws_lex_slot_type" "category" {
  description = "Available restaurant catergory"
  create_version = false
  enumeration_value {
    value = "Pick Up"
  }

  enumeration_value {
    value = "Delivery"
  }

  enumeration_value {
    value = "Delivery, Pick Up"
  }



  name                     = "category"
  value_selection_strategy = "ORIGINAL_VALUE"
}
resource "aws_lex_slot_type" "urls" {
  description = "urls"
  create_version = false
  enumeration_value {
    value = "https://stackoverflow.com/questions/44413621/custom-slot-types-in-amazon-lex-without-enumeration-values"
  }




  name                     = "urls"
  value_selection_strategy = "ORIGINAL_VALUE"
}


resource "aws_lex_slot_type" "status" {
  description = "the active status of restaurant"
  create_version = false
  enumeration_value {
    value = "true"
  }

  enumeration_value {
    value = "false"
  }


  name                     = "status"
  value_selection_strategy = "ORIGINAL_VALUE"
}
