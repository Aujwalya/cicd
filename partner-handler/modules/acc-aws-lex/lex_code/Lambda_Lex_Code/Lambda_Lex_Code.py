import json
import requests
import datetime
import time
import os
import dateutil.parser
import logging


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
#import boto3
#import pandas as pd
from io import BytesIO

url_get="https://hfz9u5otqi.execute-api.us-east-1.amazonaws.com/dev/getPartnerDetails"
url_post="https://hfz9u5otqi.execute-api.us-east-1.amazonaws.com/dev/updatePartnerDetails"
url_post2="https://hfz9u5otqi.execute-api.us-east-1.amazonaws.com/dev/addPartner"
'''
def get(partnerid,submit):
    response = requests.get(url_get)
    response.text
    response.content
    r=response.json()
    d=dict(r)
    z=d['data']
    for d in z:
        if d['id']==partnerid:
            return d[submit]
'''    
#session_attributes={}
def elicit_slot(session_attributes, intent_name, slots, slot_to_elicit, message):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ElicitSlot',
            'intentName': intent_name,
            'slots': slots,
            'slotToElicit': slot_to_elicit,
            'message': message
        }
    }

def close1(session_attributes, fulfillment_state, message):
    
    response = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': message,
            "responseCard": {
                    "version": 1,
                    "contentType": "application/vnd.amazonaws.card.generic",
                    "genericAttachments": [
              {
                 "title":"Please Choose Further Actions!",
                 "subTitle":"Please make your selection!",
               "buttons":[
                          {
                            "text":"Go update",
                            "value":"continue"
                         },
                         {
                            "text":"Exit",
                            "value":"exit"
                         }]
                }
           ]
                
            }
                    }
                    }
    return response

def close(session_attributes,fulfillment_state, message,flow_type,info="null"):
    if flow_type == "positive":
       
        response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': fulfillment_state,
                'message': message,
                "responseCard": {
                        "version": 1,
                        "contentType": "application/vnd.amazonaws.card.generic",
                        "genericAttachments": [
                  {
                     "title":"Updating!",
                     "subTitle":"What do you want to update?",
                     "buttons":[
                          {
                            "text":"Name",
                            "value":"update name"
                         },
                         {
                            "text":"Status",
                            "value":"update status"
                         },
                         {
                            "text":"Category",
                            "value":"update category"
                         },
                         {
                            "text":"Address",
                            "value":"update address"
                         },
                         {
                             "text":"More Options",
                            "value":"more options"
                         }
                         ]
                  }
                    
               ]
                    
                }
                        }
                        }
    elif flow_type == "neutral":
        response = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': message,
            "responseCard": {
                    "version": 1,
                    "contentType": "application/vnd.amazonaws.card.generic",
                    "genericAttachments": [
              {
                 "subTitle":"Please choose further actions!",
                 "buttons":[
                      {
                        "text":"Yes",
                        "value":"continue"
                     },
                     {
                        "text":"No",
                        "value":"exit"
                     }
                     ]
              }
                
           ]
            }
                    }
                    }
    elif flow_type == "sign":    
        response = {
            'sessionAttributes': session_attributes,
            'dialogAction': {
                'type': 'Close',
                'fulfillmentState': fulfillment_state,
                'message': message,
                "responseCard": {
                        "version": 1,
                        "contentType": "application/vnd.amazonaws.card.generic",
                        "genericAttachments": [
                  {
                     "title":"Updating",
                     "subTitle":"Welcome!",
                     "buttons":[
                          {
                            "text":"Register",
                            "value":"Register"
                         },
                         {
                            "text":"Exit",
                            "value":"exit"
                         }
                         ]
                  }
                    
               ]
                    
                }
                        }
                        }
    else:
        response = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': message,
            "responseCard": {
                    "version": 1,
                    "contentType": "application/vnd.amazonaws.card.generic",
                    "genericAttachments": [
              {
                 "subTitle":"Please choose further actions! ",
                "buttons":[
                      {
                        "text":"Continue",
                        "value":"continue"
                     },
                     {
                        "text":"Exit",
                        "value":"exit"
                     }
                     ]
              }
                
           ]
            }
                    }
                    }
    return response



    


def intro(intent_request):
    global session_attributes
    partnerid=intent_request['sessionAttributes']['partner_id']
    session_attributes={"partner_id":partnerid}
    #partnerid= 1
    if int(partnerid)==0:
        return signInfo(intent_request)
    else:
        final_url = '/'.join([url_get, str(partnerid)])
        response=requests.get(final_url)
        r=response.json()
        d=dict(r)
        z=d['data']
        for d in z:
            name=d["name"]
            category=d["category"]
            cuisine=d["cuisine"]
            average_cost=d["average_cost"]
            location=d["location"]
            operation_timings=d["operation_timings"]
            is_active=d["is_active"]
            address=d["address"]
    
        send1='Welcome {},i am a bot to assist you to make updates!. '.format(name)
        
        introduction="Your Current Information is as Follows:\r\nName: {}\r\nCategory: {}\r\nCuisine: {}\r\nAverage Cost: {}".format(name,category,cuisine,average_cost)
        more="\r\nLocation: {}\r\nOperation Timings: {}\r\nIs Active: {}".format(location,operation_timings,is_active)
        more2="\r\nAddress: {}\r\n".format(address)
        send=introduction+more+more2
        state="Fulfilled"
        flow_type="positive"
        message1="{\"messages\":[{\"type\":\"PlainText\",\"group\":1,\"value\":"+send1+"}]}"
        message1=json.dumps(send1)
        message2=json.dumps(send)
        return   (close1(
                session_attributes,
                state,
                {
            "contentType": "PlainText",
           #"content":"{\"messages\":[{\"type\":\"PlainText\",\"group\":1,\"value\":"+json.dumps(send1)+"},{\"type\":\"PlainText\",\"group\":1,\"value\":"+send1+"}]}"
             "content":"{\"messages\":[{\"type\":\"PlainText\",\"group\":1,\"value\":"+json.dumps(send1)+"},{\"type\":\"PlainText\",\"group\":1,\"value\":"+json.dumps(send)+"}]}"
      
                    
                }
                ))

def Continue(intent_request):
    global session_attributes
    partnerid=intent_request['sessionAttributes']['partner_id']
    session_attributes={"partner_id":partnerid}
    send='Please select your further action!'
    state="Fulfilled"
    flow_type="positive"
    
    return close(
            session_attributes,
            state,
            {
                'contentType': 'PlainText',
                'content':  send
            },
            flow_type
    )
    

def Validate_url(slots):
    url = slots['imageUrl']
    if url and not isvalid_url(url):
        return build_validation_result(
            False,
            'image_url',
            'Sorry the url entered is not  valid.Can you try a different url?'
        )

    return {'isValid': True}

def id(intent_request):
    global session_attributes

    #partnerid=int(intent_request["currentIntent"]["slots"]["id"])
    partnerid=intent_request['sessionAttributes']['partner_id']
    session_attributes={"partner_id":partnerid}
    
    



    send="Welcome  you can go and update"
    state="Fulfilled"
    flow_type="positive"

    return close(
            session_attributes,
            state,
            {
                'contentType': 'PlainText',
                'content':  send
            },
            flow_type
    )

def name(intent_request):
    global session_attributes
    #partnerid=intent_request['sessionAttributes']['partner_id']
    name=intent_request["currentIntent"]["slots"]["name"]
    id=intent_request['sessionAttributes']['partner_id']
    session_attributes={"partner_id":id}
    #id=sessionAttributes["id"]
    #response=requests.get(url_get)
    final_url = '/'.join([url_get, str(id)])
    response=requests.get(final_url)
    r=response.json()
    d=dict(r)
    z=d['data']
    for d in z:
        previous =d["name"]
    myobj = {'id':id,'column': "name","value":name}
    r=requests.post(url_post, data = myobj)
    print("now r is:",r.text)
    if r.status_code!=200:
        send="Sorry, the required updates could not be made at this moment. Please try later."
        state="Failed"
        flow_type="negative"
       
    else:
        send='Thanks, updated the name from {} to {}.'.format(previous,name)
        state="Fulfilled"
        flow_type="neutral"
    #previous_name=get(id,"name")
    return close(
        session_attributes,
        state,
        {
            'contentType': 'PlainText',
            #'content':  send
            "content":"{\"messages\":[{\"type\":\"PlainText\",\"group\":1,\"value\":"+json.dumps(send)+"},{\"type\":\"PlainText\",\"group\":1,\"value\":\"Do you want to update anything else?\"}]}"
        },
        flow_type
    )
    
    ''' 
    i=df_s3_data[df_s3_data['id']==id].index[0]
    previous_name=df_s3_data["name"].iloc[i]
    df_s3_data.loc[df_s3_data['id'] ==id , ['name']] = name
    df_s3_data.to_csv("s3://industry-x-load-test-logsbucket-pbbye7rprkk0/Book1.csv")
    '''  
def status(intent_request):
    global session_attributes
    status=intent_request["currentIntent"]["slots"]["status"]
    id=intent_request['sessionAttributes']['partner_id']
    session_attributes={"partner_id":id}
    #response=requests.get(url_get)
    final_url = '/'.join([url_get, str(id)])
    response=requests.get(final_url)
    r=response.json()
    d=dict(r)
    z=d['data']
    for d in z:
        previous =d["is_active"]
    myobj = {'id':id,'column': "is_active","value":status}
    r=requests.post(url_post, data = myobj)
    
    if r.status_code!=200:
        send="Sorry, the required updates could not be made at this moment. Please try later."
        state="Failed"
        flow_type="negative"
       
    else:
        send='Thanks, updated active status from {} to {}.'.format(previous,status)
        state="Fulfilled"
        flow_type="neutral"
    
    #previous_status=get(id,"is_active")
    return close(
        session_attributes,
        state,
        {
            'contentType': 'PlainText',
            #'content':  send
            "content":"{\"messages\":[{\"type\":\"PlainText\",\"group\":1,\"value\":"+json.dumps(send)+"},{\"type\":\"PlainText\",\"group\":1,\"value\":\"Do you want to update anything else?\"}]}"
        },
        flow_type
    )
    

def average(intent_request):
    average=intent_request["currentIntent"]["slots"]["average"]
    global session_attributes
    id=intent_request['sessionAttributes']['partner_id']
    session_attributes={"partner_id":id}
    #response=requests.get(url_get)
    final_url = '/'.join([url_get, str(id)])
    response=requests.get(final_url)
    r=response.json()
    d=dict(r)
    z=d['data']
    for d in z:
        previous =d["average_cost"]
    myobj = {'id':id,'column': "average_cost","value":average}
    r=requests.post(url_post, data = myobj)
    if r.status_code!=200:
        send="Sorry, the required updates could not be made at this moment. Please try later."
        state="Failed"
        flow_type="negative"
       
    else:
        send='Thanks, updated the average cost from {} to {}.'.format(previous,average)
        state="Fulfilled"
        flow_type="neutral"
    return close(
        session_attributes,
        state,
        {
            'contentType': 'PlainText',
            #'content':  send
            "content":"{\"messages\":[{\"type\":\"PlainText\",\"group\":1,\"value\":"+json.dumps(send)+"},{\"type\":\"PlainText\",\"group\":1,\"value\":\"Do you want to update anything else?\"}]}"
        },
        flow_type
    ) 
    

def address(intent_request):
    address=intent_request["currentIntent"]["slots"]["address"]
    global session_attributes
    id=intent_request['sessionAttributes']['partner_id']
    session_attributes={"partner_id":id}
    #response=requests.get(url_get)
    final_url = '/'.join([url_get, str(id)])
    response=requests.get(final_url)
    r=response.json()
    d=dict(r)
    z=d['data']
    for d in z:
        previous =d["address"]
    myobj = {'id':id,'column': "address","value":address}
    r=requests.post(url_post, data = myobj)
    #previous_address=get(id,"address")
    if r.status_code!=200:
        send="Sorry, the required updates could not be made at this moment. Please try later."
        state="Failed"
        flow_type="negative"
       
    else:
        send='Thanks, updated the address from {} to {}.'.format(previous,address)
        state="Fulfilled"
        flow_type="neutral"
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            #'content':  send
            "content":"{\"messages\":[{\"type\":\"PlainText\",\"group\":1,\"value\":"+json.dumps(send)+"},{\"type\":\"PlainText\",\"group\":1,\"value\":\"Do you want to update anything else?\"}]}"
        },
        flow_type
    )
    

def category(intent_request):
    global session_attributes
    category=intent_request["currentIntent"]["slots"]["category"]
    id=intent_request['sessionAttributes']['partner_id']
    session_attributes={"partner_id":id}
    #response=requests.get(url_get)
    final_url = '/'.join([url_get, str(id)])
    response=requests.get(final_url)
    r=response.json()
    d=dict(r)
    z=d['data']
    for d in z:
        previous =d["category"]
    myobj = {'id':id,'column': "category","value":category}
    r=requests.post(url_post, data = myobj)
    if r.status_code!=200:
        send="Sorry, the required updates could not be made at this moment. Please try later."
        state="Failed"
        flow_type="negative"   
    else:
        send='Thanks, updated category from {} to {}.'.format(previous,category)
        state="Fulfilled"
        flow_type="neutral"
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            #'content':  send
            "content":"{\"messages\":[{\"type\":\"PlainText\",\"group\":1,\"value\":"+json.dumps(send)+"},{\"type\":\"PlainText\",\"group\":1,\"value\":\"Do you want to update anything else?\"}]}"
        },
        flow_type
    )
    

def cuisine(intent_request):
    global session_attributes
    cuisine=intent_request["currentIntent"]["slots"]["cuisine"]
    id=intent_request['sessionAttributes']['partner_id']
    session_attributes={"partner_id":id}
    #response=requests.get(url_get)
    final_url = '/'.join([url_get, str(id)])
    response=requests.get(final_url)
    r=response.json()
    d=dict(r)
    z=d['data']
    for d in z:
        previous =d["cuisine"]
    myobj = {'id':id,'column': "cuisine","value":cuisine}
    r=requests.post(url_post, data = myobj)
    if r.status_code!=200:
        send="Sorry, the required updates could not be made at this moment. Please try later."
        state="Failed"
        flow_type="negative"   
    else:
        send='Thanks,updated Cuisine from {} to {}.'.format(previous,cuisine)
        state="Fulfilled"
        flow_type="neutral"
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            #'content':  send
            "content":"{\"messages\":[{\"type\":\"PlainText\",\"group\":1,\"value\":"+json.dumps(send)+"},{\"type\":\"PlainText\",\"group\":1,\"value\":\"Do you want to update anything else?\"}]}"
        },
        flow_type
    )
    

def operation_timings(intent_request):
    global session_attributes
    start_time=intent_request["currentIntent"]["slots"]["start_time"]
    end_time=intent_request["currentIntent"]["slots"]["end_time"]
    id=intent_request['sessionAttributes']['partner_id']
    session_attributes={"partner_id":id}
    #response=requests.get(url_get)
    final_url = '/'.join([url_get, str(id)])
    response=requests.get(final_url)
    r=response.json()
    d=dict(r)
    z=d['data']
    for d in z:
        previous =d["operation_timings"]
    operation_timings=start_time+" "+end_time
    myobj = {'id':id,'column': "operation_timings","value":operation_timings}
    r=requests.post(url_post, data = myobj)
    if r.status_code!=200:
        send="Sorry, the required updates could not be made at this moment. Please try later."
        state="Failed"
        flow_type="negative"   
    else:
        send='Thanks,updated your operating timings from {} to {}.'.format(previous,operation_timings)
        state="Fulfilled"
        flow_type="neutral"
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            #'content': send 
            "content":"{\"messages\":[{\"type\":\"PlainText\",\"group\":1,\"value\":"+json.dumps(send)+"},{\"type\":\"PlainText\",\"group\":1,\"value\":\"Do you want to update anything else?\"}]}"
        },
        flow_type
    )
    

def location(intent_request):
    global session_attributes
    location=intent_request["currentIntent"]["slots"]["location"]
    #id=session_attributes["id"]
    id=intent_request['sessionAttributes']['partner_id']
    session_attributes={"partner_id":id}
    #response=requests.get(url_get)
    final_url = '/'.join([url_get, str(id)])
    response=requests.get(final_url)
    r=response.json()
    d=dict(r)
    z=d['data']
    for d in z:
        previous =d["location"]
    myobj = {'id':id,'column': "location","value":location}
    r=requests.post(url_post, data = myobj)
    if r.status_code!=200:
        send="Sorry, the required updates could not be made at this moment. Please try later."
        state="Failed"
        flow_type="negative"
       
    else:
        send='Thanks,updated location from {} to {}.'.format(previous,location)
        state="Fulfilled"
        flow_type="neutral"
        
    return close(
            session_attributes,
            state,
            {
                'contentType': 'PlainText',
                #'content':  send
                "content":"{\"messages\":[{\"type\":\"PlainText\",\"group\":1,\"value\":"+json.dumps(send)+"},{\"type\":\"PlainText\",\"group\":1,\"value\":\"Do you want to update anything else?\"}]}"
            },
            flow_type
    )
    
def image_url(intent_request):
    global session_attributes
    id=session_attributes["partner_id"]
    session_attributes={"partner_id":id}
    image_url=intent_request["currentIntent"]["slots"]["imageUrl"]
    print(image_url)
    r = requests.get(image_url)
    print(r.status_code)
    '''
    r = requests.get(image_url)
    if r.status_code!=200:
        slots = intent_request['currentIntent']['slots']
        slots[validation_result['violatedSlot']]=0
        return elicit_slot(
            session_attributes,
            intent_request['currentIntent']['name'],
            slots,
            validation_result['violatedSlot'],
            validation_result['message']
        )
    else:
        #id=session_attributes["id"]
        myobj = {'id':id,'column': "image_url","value":image_url}
        r=requests.post(url_post, data = myobj)
        #previous_address=get(id,"address")
    '''
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content':  'Thanks,updated image_url to {}.\r\nDo you want to update anything else?'.format(image_url)
        }
    )

def more_options(intent_request):
    global session_attributes
    id=session_attributes["partner_id"]
    session_attributes={"partner_id":id}
    response = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': "Fulfilled",
            'message':{
            'contentType': 'PlainText',
            'content':  'There are more options'
        },
            "responseCard": {
                "version": 1,
                "contentType": "application/vnd.amazonaws.card.generic",
                "genericAttachments": [
          {
             "title":"More options",
             "subTitle":"More Options to make updates",
            "buttons":[
                  {
                    "text":"average",
                    "value":"update average"
                 },
                 {
                     "text":"cuisine",
                    "value":"update cuisine"
                 },
                 {
                     "text":"operation_timings",
                    "value":"update operating time"
                 },
                 {
                     "text":"location",
                    "value":"update location"
                 }
                 ]
          }
            
       ]
            
        }
        }
       }
    return response



def signInfo(intent_request):
    id=intent_request['sessionAttributes']['partner_id']
    session_attributes={"partner_id":id}
    return close(
        session_attributes,
        'Fulfilled',
        {
            'contentType': 'PlainText',
            'content':  'Welcome to Partner Onboard you can register here(/"Already Registered Login Please/")'
        },"sign"
    )
def build_validation_result(isvalid, violated_slot, message_content):
    return {
        'isValid': isvalid,
        'violatedSlot': violated_slot,
        'message': {'contentType': 'PlainText', 'content': message_content}
    }
def isvalid_city(addlocation):
    valid_cities = ['delhi','Uttar pradesh','haryana','Goa','andhra pradesh']
    return addlocation.lower() in valid_cities

def validate_partner(slots):
    addlocation=slots['addlocation']
    addimgUrl=slots['addimgUrl']
    if addlocation and not isvalid_city(addlocation):
        return build_validation_result(
            False,
            'addlocation',
            'We currently do not support {} as a valid destination.  Can you try a different Location'.format(addlocation)
        )

    
    return {'isValid': True}

   
def addPartner(intent_request):
    addname=intent_request['currentIntent']['slots']['addname']
    addcategory=intent_request['currentIntent']['slots']['addcategory']
    addcuisine=intent_request['currentIntent']['slots']['addcuisine']
    addaverageCost=intent_request['currentIntent']['slots']['addaverageCost']
    addlocation=intent_request['currentIntent']['slots']['addlocation']
    addaddress=intent_request['currentIntent']['slots']['addaddress']
    addisActive=intent_request['currentIntent']['slots']['addisActive']
    addimgUrl=intent_request['currentIntent']['slots']['addimgUrl']
    addstart_time=intent_request['currentIntent']['slots']['addstart_time']
    addend_time=intent_request['currentIntent']['slots']['addend_time']
    #if intent_request['invocationSource'] == 'DialogCodeHook':
    #    # Validate any slots which have been specified.  If any are invalid, re-elicit for their value
    validation_result = validate_partner(intent_request['currentIntent']['slots'])
    while not validation_result['isValid']:
        slots = intent_request['currentIntent']['slots']
        slots[validation_result['violatedSlot']] = None

        return elicit_slot(
                session_attributes,
                intent_request['currentIntent']['name'],
                slots,
                validation_result['violatedSlot'],
                validation_result['message']
        )

        # Otherwise, let native DM rules determine how to elicit for slots and prompt for confirmation.  Pass price
        # back in sessionAttributes once it can be calculated; otherwise clear any setting from sessionAttributes.
    
    timings=addstart_time+" "+ addend_time
    myobj ={"name":addname,"category":addcategory,"cuisine":addcuisine,"averageCost":addaverageCost,"location":addlocation,"address":addaddress,"operationTimings":timings,"isActive":addisActive,"imgUrl":addimgUrl}
    r=requests.post(url_post2, data = myobj)
    
    if r.status_code==200:
        
        response = {
            'dialogAction': {
             'type': 'Close',
                'fulfillmentState': "Fulfilled",
                'message':{
                    'contentType': 'PlainText',
                    'content':"Registered Successfully!! You can go and Login",
            } 
                
                        }
                        }
    else:
        response = {
            'dialogAction': {
             'type': 'Close',
                'fulfillmentState': "Fulfilled",
                'message':{
                    'contentType': 'PlainText',
                    'content':"Please Try Again Later",
            } 
                
                        }
                        }
    return response

    
def dispatch(intent_request):
    """
    Called when the user specifies an intent for this bot.
    """

#   logger.debug('dispatch userId={}, intentName={}'.format(intent_request['userId'], intent_request['currentIntent']['name']))
    #print(intent_request['sessionAttributes']['partner_id'])
    intent_name = intent_request['currentIntent']['name']
    #z=int(intent_request['sessionAttributes']['partner_id'])
    if int(intent_request['sessionAttributes']['partner_id'])>=0:
        
        # Dispatch to your bot's intent handlers
        if intent_name == 'Introduce_Partner':
            return intro(intent_request)
        elif intent_name == 'Validate_id':
            return id(intent_request)
        elif intent_name == 'update_Name':
            return name(intent_request)
        elif intent_name == 'update_Status':
            return status(intent_request)
        elif intent_name=='update_Average':
            return average(intent_request)
        elif intent_name =='update_Address':
            return address(intent_request)
        elif intent_name=='update_Category':
            return category(intent_request)
        elif intent_name=='update_Cuisine':
            return cuisine(intent_request)
        elif intent_name=='updateoperation_timings':
            return operation_timings(intent_request)
        elif intent_name=='update_location':
            return location(intent_request)
        elif intent_name=='updateimage_url':
            return image_url(intent_request)   
        elif intent_name=='more_Options':
            return more_options(intent_request)
        elif intent_name=='Continue_updating':
            return Continue(intent_request)
        elif intent_name=='addPartner':
            return addPartner(intent_request)
    #elif int(intent_request['sessionAttributes']['partner_id'])==0:
    #   if intent_name=='SignUpIntro':
    #        return signInfo(intent_request)
    #raise Exception('Intent with name ' + intent_name + ' not supported')
    


# --- Main handler ---


def lambda_handler(event, context):
    """
    Route the incoming request based on intent.
    The JSON body of the request is provided in the event slot.
    """
    # By default, treat the user request as coming from the America/New_York time zone.
    os.environ['TZ'] = 'America/New_York'
    time.tzset()
    logger.debug('event.bot.name={}'.format(event['bot']['name']))
    logger.debug(event)
    #print(event['sessionAttributes']['partner_id'])
    return dispatch(event)