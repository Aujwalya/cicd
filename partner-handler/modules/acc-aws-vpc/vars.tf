variable "vpc_cidr" {
    default = "10.1.0.0/16"
}

# variable "tenancy" {
#     # default = "dedicatd"
# }

variable "vpc_id"{}

variable "subnet_cidr" {
    default = "10.0.1.0/24"
}
variable "subnet_cidr1" {
    default = "10.0.1.1/24"
}

variable "cidr_subnet" {
  description = "CIDR block for the subnet"
  default     = "10.1.0.0/24"
}
variable "availability_zone" {
  description = "availability zone to create subnet"
  default     = "us-east-1a"
}

variable "environment_tag" {
  description = "Environment tag"
  default     = "Production"
}

variable "sg"{}
variable "sg_id"{}