resource "aws_vpc" "main" {
  cidr_block       = "${var.vpc_cidr}"
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "main"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id
  tags = {
    "Environment" = var.environment_tag
  }
}

resource "aws_subnet" "main" {
  availability_zone =var.availability_zone
  vpc_id     = aws_vpc.main.id #"${var.vpc_id}"
  cidr_block = "${var.subnet_cidr1}"
map_public_ip_on_launch = "true"
  tags = {
    Name = "Main"
  }
}
resource "aws_subnet" "main1" {
  availability_zone ="us-east-1c"
  vpc_id     = aws_vpc.main.id #"${var.vpc_id}"
  cidr_block = "${var.subnet_cidr}"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "Main2"
  }
}

resource "aws_route_table" "rtb_public" {
  vpc_id = aws_vpc.main.id

  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    "Environment" = var.environment_tag
  }
}

resource "aws_route_table_association" "rta_subnet_public" {
  subnet_id      = aws_subnet.main.id
  route_table_id = aws_route_table.rtb_public.id
}

#To create new Security Group
resource "aws_security_group" "AkPostTable" {
name = "AkPostTable"
description = "security group for terraAk"
vpc_id      = aws_vpc.main.id

ingress {
from_port = 8080
to_port = 8080
protocol = "tcp"
cidr_blocks = ["0.0.0.0/0"]
 }
ingress {
from_port = 22
to_port = 22
protocol = "tcp"
cidr_blocks = ["0.0.0.0/0"]
 }
ingress {
from_port = 5432
to_port = 5432
protocol = "tcp"
cidr_blocks = ["0.0.0.0/0"]
 }
ingress {
from_port = 27017
to_port = 27018
protocol = "tcp"
cidr_blocks = ["0.0.0.0/0"]
 }
egress {
from_port = 0
to_port = 0
protocol = "-1"
cidr_blocks = ["0.0.0.0/0"]
 }
tags = {
Name = "AkPostTable"
 }
}

output "vpc_id" {
  value = "${aws_vpc.main.id}"
}

output "subnet_id" {
  value = "${aws_subnet.main.id}"
}
output "subnet_id2" {
  value = "${aws_subnet.main1.id}"
}


output "sg" {
  value = "${aws_security_group.AkPostTable.name}"
}

output "sg_id" {
  value = "${aws_security_group.AkPostTable.id}"
}

# output "sg_id" {
#   value = "${aws_security_group.AkPostTable.id}"
# }
# resource "aws_security_group" "allow_tls" {
#   name        = "allow_tls"
#   description = "Allow TLS inbound traffic"
#   vpc_id      = aws_vpc.main.id

#   ingress {
#     description = "TLS from VPC"
#     from_port   = 443
#     to_port     = 443
#     protocol    = "tcp"
#     cidr_blocks = [aws_vpc.main.cidr_block]
#   }

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   tags = {
#     Name = "allow_tls"
#   }
# }

