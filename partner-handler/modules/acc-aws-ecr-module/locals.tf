locals {
    # common_tags = merge(var.platform_output.tags, {
    #   "Owner"       = var.platform_output.owner,
    #   "Environment" = var.platform_output.environment_type
    # })

  ecr_name  = "${var.ecr_name}-ecr"
  # image_tag_mutability = "IMMUTABLE"
  # ecr_image_tag_mutability  = "${var.ecr_image_tag_mutability}"
  ecr_image_tag_mutability  = var.ecr_image_tag_mutability
}