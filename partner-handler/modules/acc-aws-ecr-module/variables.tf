variable "ecr_name" {
description = "variable for ECR module."
type = string
default = "ecr1002"
}

variable "ecr_image_tag_mutability" {
  description = "value to tag mutability"
  type = string
  default = "IMMUTABLE"
}


# variable "ecr-name" {
#     type = string
# }
#     # description =   "variable for ECR module."
#     # type        =   string
#     # default = "ecr1"
# # }

# variable "ecr-image-tag-mutability" {
#     type = string
#     default = "IMMUTABLE"
#  }
#     # description =   "value to tag mutability"
#     # type        =   string
#     # default = "IMMUTABLE"
# # }

