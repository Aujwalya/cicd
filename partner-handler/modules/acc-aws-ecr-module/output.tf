output "ecr_output" {
  description = "It will provide the output for AWS ECR."
  value = {
    registry_id = aws_ecr_repository.demo-repository.registry_id
    arn =  aws_ecr_repository.demo-repository.arn 
    repository_url  = aws_ecr_repository.demo-repository.repository_url
  }
}

output "mpp-report" {
  description = "It will provide the output for AWS ECR."
  value = {
    registry_id = aws_ecr_repository.demo-repository.registry_id
    arn =  aws_ecr_repository.demo-repository.arn 
    repository_url  = aws_ecr_repository.demo-repository.repository_url
  }
}