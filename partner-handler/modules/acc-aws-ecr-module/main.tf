# provider "aws" {
#   access_key = ""
#   secret_key = ""
#   region     = "us-east-1"
# }

#AWS ECR Resource
# resource "aws_ecr_repository" "demo-repository" {
#   name                 = "${var.name}"
#   image_tag_mutability = "${var.image_tag_mutability}"

#   # resource "aws_ecr_repository" "demo-repository" {
#   # name                 = "bar"
#   # image_tag_mutability = "MUTABLE"

#   image_scanning_configuration {
#     scan_on_push = true
#   }
# }

resource "aws_ecr_repository" "demo-repository" {
  name                 = local.ecr_name
  image_tag_mutability = local.ecr_image_tag_mutability
}

  #add and edit tag here and remove this comment.
  # tags = merge(local.common_tags, {
  #   Name = "${var.platform_output.system_name}-bastion-host-${var.platform_output.environment_type}"
  # })
# }
resource "aws_ecr_repository_policy" "demo-repo-policy" {
  repository = aws_ecr_repository.demo-repository.name
  policy     = <<EOF
  {
    "Version": "2008-10-17",
    "Statement": [
      {
        "Sid": "adds full ecr access to the demo repository",
        "Effect": "Allow",
        "Principal": "*",
        "Action": [
          "ecr:BatchCheckLayerAvailability",
          "ecr:BatchGetImage",
          "ecr:CompleteLayerUpload",
          "ecr:GetDownloadUrlForLayer",
          "ecr:GetLifecyclePolicy",
          "ecr:InitiateLayerUpload",
          "ecr:PutImage",
          "ecr:UploadLayerPart"
        ]
      }
    ]
  }
  EOF
}

# output "name" {
#   value = "${aws_ecr_repository.demo-repository.name}"
# }

# output "image-tag-mutability" {
#   value = "${aws_ecr_repository.demo-repository.image_tag_mutability}"
# }