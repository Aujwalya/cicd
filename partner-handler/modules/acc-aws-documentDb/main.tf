
resource "aws_docdb_cluster" "docdb" {
  cluster_identifier      = "my-docdb-cluster"
  engine                  = "docdb"
  master_username         = "corona"
  master_password         = "gocorona"
  backup_retention_period = 5
  preferred_backup_window = "07:00-09:00"
  skip_final_snapshot     = true
  db_subnet_group_name = aws_db_subnet_group.DocumentDB_subnet_group.name
  vpc_security_group_ids = ["${var.sg_id}"]
}
resource "aws_docdb_cluster_instance" "cluster_instances" {

  cluster_identifier = aws_docdb_cluster.docdb.id
  instance_class     = "db.t3.medium"
}


resource "aws_db_subnet_group" "DocumentDB_subnet_group" {
    name       = "documentdb"
    subnet_ids = [ "${var.subnet_id}", "${var.subnet_id2}"]

    tags = {
        Name = "DocumentDB subnet group"
    }
}
variable "sg_id" {
}
variable "subnet_id" {
  
}
variable "subnet_id2" {
  
}
output "endpoint" {
  value = "${aws_docdb_cluster.docdb.endpoint}"
  
}
output "master_username" {
  value = "${aws_docdb_cluster.docdb.master_username}"
  
}
output "master_password" {
  value = "${aws_docdb_cluster.docdb.master_password}"
  
}
output "inst-endpoint" {
  value = "${aws_docdb_cluster_instance.cluster_instances.endpoint}"
}



resource "null_resource" "serverless-on-boarding" {
   
   connection {
      host        = "${var.aws_instance}"
      timeout     = "30m"
      user        = "ec2-user"
      type        = "ssh" 
      port        = 22
      # agent       = true
      private_key = file("../modules/acc-aws-keyPair/partner-handler.pem")
    }

provisioner "file" {
    source      = "../modules/acc-aws-documentDb/test/"
    destination = "/home/ec2-user/"

}
 provisioner "remote-exec" {
     
    inline = [

       "echo ${aws_docdb_cluster_instance.cluster_instances.endpoint}",

"wget https://s3.amazonaws.com/rds-downloads/rds-combined-ca-bundle.pem",
"sudo yum install python3 -y",
"sudo yum install python3-pip3 -y",
"sudo pip3 install --upgrade pip", 

      "python3 -m pip install pymongo",
"python3 tests.py ${aws_docdb_cluster.docdb.master_username} ${aws_docdb_cluster.docdb.master_password} ${aws_docdb_cluster.docdb.endpoint}"




   ]
 } 
 }
variable "aws_instance" {
  
}