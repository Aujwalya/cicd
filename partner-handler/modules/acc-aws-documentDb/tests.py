#! /usr/bin/python
import os
import sys
import pymongo

#Insert sample data
SEED_DATA = [{
        "_id": 1,
        "partner_id": 1,
        "partner_name": "Behrouz Biryani",
        "slug": "behrouzbiryani",
        "partner_cuisine": ["Biryani", "North Indian", "Mughlai"],
        "is_active": "true",
        "is_veg": "false",
        "menu": [
            {
                "item_id": 101,
                "item_name": "Paneer Biryani",
                "item_description": "An easy and simple dum styled cooked briyani receipe made with marinated paneer cubes and long grain rice",
                "item_cost": 250,
                "item_rating": 4.2,
                "in_stock": "true",
                "image_url": "https://images.pexels.com/photos/2624774/pexels-photo-2624774.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
                "is_veg": "true"
            },
            {
                "item_id": 103,
                "item_name": "Chicken Biryani",
                "item_description": "A delicious savory rice dish that is loaded with spicy marinated chicken, caramelized onions, and flavorful saffron rice.",
                "item_cost": 350,
                "item_rating": 4.7,
                "in_stock": "true",
                "image_url": "https://images.pexels.com/photos/1624487/pexels-photo-1624487.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
                "is_veg": "true"
            }
        ]
    },
    {
        "_id": 2,
        "partner_id": 2,
        "partner_name": "Food Plaza",
        "slug": "foodplaza",
        "partner_cuisine": ["North Indian", "Chinese"],
        "is_active": "true",
        "is_veg": "true",
        "menu": [
            {
                "item_id": 1,
                "item_name": "Paneer Tikka Masala",
                "item_description": "Marinated paneer cheese served in a spiced gravy.",
                "item_cost": [
                    {
                        "type": "Regular",
                        "cost": 200
                    }
                ],
                "item_rating": 4.2,
                "in_stock": "true",
                "image_url": "https://images.pexels.com/photos/3928854/pexels-photo-3928854.png?auto=compress&cs=tinysrgb&dpr=2&w=500",
                "is_veg": "true"
            },
            {
                "item_id": 2,
                "item_name": "Shahi Paneer",
                "item_description": "Thick gravy of cream, tomatoes and Indian spices",
                "item_cost": [
                    {
                        "type": "Regular",
                        "cost": 190
                    }
                ],
                "item_rating": 4.1,
                "in_stock": "true",
                "image_url": "https://images.pexels.com/photos/2580464/pexels-photo-2580464.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500",
                "is_veg": "true"
            },
            {
                "item_id": 3,
                "item_name": "Paneer Butter Masala",
                "item_description": "Rich and creamy dish of paneer in a tomato, butter and cashew sauce",
                "item_cost": [
                    {
                        "type": "Regular",
                        "cost": 230
                    }
                ],
                "item_rating": 4.3,
                "in_stock": "true",
                "image_url": "https://images.pexels.com/photos/2474661/pexels-photo-2474661.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
                "is_veg":"true"
            }
        ]
    }

]

# clusterendpoint = "mongodb://sys.argv[0]:sys.argv[1]@sys.argv[2]:27017/?ssl=true&ssl_ca_certs=rds-combined-ca-bundle.pem&replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false"
clusterendpoint="mongodb://{}:{}@{}:27017/?ssl=true&ssl_ca_certs=rds-combined-ca-bundle.pem&replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false".format(sys.argv[1],	sys.argv[2],sys.argv[3])
def main(args):
    #Establish DocumentDB connection
    client = pymongo.MongoClient(clusterendpoint,retryWrites='true')
    db = client.sample_database
    profiles = db['profiles']

    #Insert data
    profiles.insert_many(SEED_DATA)
    print("Successfully inserted data")

    client.close()

if __name__ == '__main__':
    main(sys.argv[1:])