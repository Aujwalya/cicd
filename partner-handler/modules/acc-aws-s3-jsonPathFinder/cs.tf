locals {
  bucket_name = "cloud-front-json-path-finders-s1"
}



output "endpoint"{
  value=aws_s3_bucket.main.website_endpoint
}
output "Bucket_name"{
  value=local.bucket_name
}

variable "mime_types" {
  default = {
    htm = "text/html"
    html = "text/html"
    css = "text/css"
    js = "application/javascript"
    map = "application/javascript"
    json = "application/json"
  }
}
# partner-handler\modules\s3\myfiles
resource "aws_s3_bucket_object" "object1" {

for_each = fileset("../modules/s3/myfiles/", "*")
bucket = aws_s3_bucket.main.id
key = each.key
content_type = "${lookup(var.mime_types , "${split(".", each.value)[1]}" )}"
source = "../modules/s3/myfiles/${each.value}"
 }
# # Refactor it to use loop
# resource "aws_s3_bucket_object" "error" {
#   bucket       = local.bucket_name
#   key          = "error.html"
#   source       = "../modules/s3/error.html"
#   content_type = "text/html"
# }


# resource "aws_s3_bucket_object" "index" {
#   bucket       = local.bucket_name
#   key          = "index.html"
#   source       = "../modules/s3/index.html"
#   content_type = "text/html"
# }
# resource "aws_s3_bucket_object" "ace" {
# bucket = local.bucket_name
# key = "ace.js"
# content_type = "application/javascript"
# source = "../modules/s3/ace.js"
# }



# resource "aws_s3_bucket_object" "object3" {
# bucket = local.bucket_name
# key = "mode-json.js"
# content_type = "application/javascript"
# source = "../modules/s3/mode-json.js"
# }
# resource "aws_s3_bucket_object" "object4" {
# bucket = local.bucket_name
# key = "style.css"
# content_type = "text/css"
# source = "../modules/s3/style.css"
# }
# resource "aws_s3_bucket_object" "object5" {
# bucket = local.bucket_name
# key = "theme-chrome.js"
# content_type = "application/javascript"
# source = "../modules/s3/theme-chrome.js"
# }
# resource "aws_s3_bucket_object" "object6" {
# bucket = local.bucket_name
# key = "worker-json.js"
# content_type = "application/javascript"
# source = "../modules/s3/worker-json.js"
# }




resource "aws_s3_bucket" "main" {
  bucket = local.bucket_name
  acl    = "public-read"
  policy = data.aws_iam_policy_document.bucket_policy.json

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  force_destroy ="false"

  tags = {
    "Name" = local.bucket_name
  }



}

data "aws_iam_policy_document" "bucket_policy" {
  statement {
    sid = "AllowReadFromAll"

    actions = [
      "s3:GetObject",
    ]

    resources = [
      "arn:aws:s3:::${local.bucket_name}/*",
    ]

    principals {
      type        = "*"
      identifiers = ["*"]
    }
  }
}