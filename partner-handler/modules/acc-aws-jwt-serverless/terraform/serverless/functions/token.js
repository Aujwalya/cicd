const jwt = require("jsonwebtoken");

module.exports.handler = async function signToken() {
  try {
    const secret = Buffer.from(process.env.JWT_SECRET, "base64");

    const token = jwt.sign({ username: 'test', role: 'admin' }, secret, {
      expiresIn: process.env.EXPIRY_TIME || '900s'
    });

    return {
      statusCode: 200,
	  headers: {
          "Access-Control-Allow-Headers" : "Content-Type",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
      },
      body: JSON.stringify(token)
    }
  }
  catch(err) {
    return {
      statusCode: 500,
      body: JSON.stringify(err)
    }
  }

};
