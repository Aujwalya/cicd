  variable "ec2_count" {
      default = "1"
  }

#   variable "ami_id"{}

#   variable "instance_type" {
#       default = "t2.micro"
#   }

  variable "subnet_id" {}

  variable "instance_ami" {
  description = "AMI for aws EC2 instance"
  default     = "ami-0742b4e673072066f"
}

variable "instance_type" {
  description = "type for aws EC2 instance"
  default     = "t2.micro"
}