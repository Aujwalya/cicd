resource "aws_instance" "web" {
  ami           = var.instance_ami
  instance_type = var.instance_type
  subnet_id     = "${var.subnet_id}"
  vpc_security_group_ids = [var.sg_id]
  key_name = "partner-handler"
  tags = {
    Name = "Main"
  }
}

variable "sg_id" {}
output aws_instance {
  value = aws_instance.web.public_ip
}

output "ami_id" {
  value = aws_instance.web.ami
}