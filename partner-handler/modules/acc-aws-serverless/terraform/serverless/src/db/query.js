const pool = require('../config/config').pool;
const { response, request } = require('express');
const utility = require(`../../utility`);
const format = require('pg-format');
var md5 = require('md5');

const getPartnerDetails = (request, response, next) => {
  try {
    queryString = 'SELECT * FROM partnerhandlerschema.partners WHERE id = $1' 
		pool.query(queryString, [request.params.id], (error, results) => {
			if (error) {
				next(error)
			} else {
				response.status(200).json({'status': 'success', 'message': '', 'count': results.rows.length, 'data' : results.rows})
			}
		})
  }
  catch(err) {
    next(err);
  }
}

const getPartnerEndpointInfo = (request, response, next) => {
  try {
    pool.query('Select id as partnerEndpointId, display_name, url from partnerhandlerschema.partner_endpoints where endpoint_id = $1 and partner_id = $2', 
    [request.params.sourceEndpointId, request.params.partnerId], (error, results) => {
      console.log(error, results);
      if (error) {
        next(error);
      } else {
        response.json({ message: 'success', count: results.rows.length, data: results.rows})
      }
    })
  } catch(err) {
    next(err);
  }

}

const getPartnerMappings = (request, response, next) => {
  try {
    let queryString;
    if (request.params.type === 'request') {
      queryString = `SELECT A.id as mappingId, key_name, mapping as mapping_value 
                    FROM partnerhandlerschema.request_mappings as A 
                    INNER JOIN partnerhandlerschema.source_endpoint_request_keys as B
                    ON A.key_id = B.id WHERE partner_endpoint_id = $1;`
    } else if (request.params.type === 'response') {
      queryString = `SELECT A.id as mappingId, key_name, mapping_value 
      FROM partnerhandlerschema.response_mappings as A 
      INNER JOIN partnerhandlerschema.source_endpoint_response_keys as B
      ON A.key_id = B.id WHERE partner_endpoint_id = $1;`
    }
    pool.query(queryString, [request.params.partnerEndpointId], (error, results) => {
      if (error) {
        next(error);
      } else {
        response.json({ message: 'success', count: results.rows.length, data: results.rows})
      }
    })
  } catch(err) {
    next(err);
  }
}

const getKeysbyEndpoint = (request, response, next) => {
  try {
    let queryString;
    if (request.params.type === 'request') {
      queryString = 'SELECT id,key_name FROM partnerhandlerschema.source_endpoint_request_keys WHERE source_endpoint_id = $1' 
    } else if (request.params.type === 'response') {
      queryString = 'SELECT id,key_name FROM partnerhandlerschema.source_endpoint_response_keys WHERE source_endpoint_id = $1' 
    }
		pool.query(queryString, [request.params.id], (error, results) => {
			if (error) {
				next(error)
			} else {
				response.status(200).json({'status': 'success', 'message': '', 'count': results.rows.length, 'data' : results.rows})
			}
		})
  }
  catch(err) {
    next(err)
  }
}

const getAvailableEndpoints = (request, response, next) => {
	try {
		pool.query('SELECT id,name FROM partnerhandlerschema.source_endpoints', (error, results) => {
			if (error) {
				next(error)
			} else {
				response.status(200).json({'status': 'success', 'message': '', 'count': results.rows.length, 'data' : results.rows})
			}
		})
	}
	catch(err) {
		next(err)
	}

}

const createPartner = (request, response) => {
  let [
    name,
    category,
    cuisine,
    averageCost,
    rating,
    location,
    operationTimings,
    isActive,
    createdOn,
    updatedOn,
    imgUrl,
    address
  ] = [
    utility.getObject(request, "body.name", ""),
    utility.getObject(request, "body.category", ""),
    utility.getObject(request, "body.cuisine", ""),
    utility.getObject(request, "body.averageCost", 0),
    utility.getObject(request, "body.rating", 0),
    utility.getObject(request, "body.location", ""),
    utility.getObject(request, "body.operationTimings", ""),
    utility.getObject(request, "body.isActive", false),
    utility.getObject(request, "body.createdOn", new Date(Date.now()).toISOString()),
    utility.getObject(request, "body.updatedOn", null),
    utility.getObject(request, "body.imgUrl", null),
    utility.getObject(request, "body.address", null),
  ];

  pool.query(
    `INSERT INTO partnerhandlerschema.partners
     (name, category, cuisine, average_cost, rating, 
      location, operation_timings, is_active, created_on,
      updated_on, image_url, address) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) 
      RETURNING id`,
    [
      name,
      category,
      cuisine,
      averageCost,
      rating,
      location,
      operationTimings,
      isActive,
      createdOn,
      updatedOn,
      imgUrl,
      address,
    ],
    (error, results) => {
      if (error) {
        throw error;
      }

      pool.query(`UPDATE partnerhandlerschema.users 
      SET partner_id = $1
      WHERE user_email = $2`, [results.rows[0].id, request.body.email], (err, res) => {
        if (err) {
          throw err;
        }
        response.json({
          status: "success",
          count: results.rowCount,
          message: `Partner has been added succesfully.`,
          id: results.rows[0].id,
        });
      })
    }
  );
};

const updatePartnerDetails = (request, response) => {
  let [
    id,
    column,
    value
  ] = [
    utility.getObject(request, "body.id"),
    utility.getObject(request, "body.column"),
    utility.getObject(request, "body.value")
  ];
  const sql = format('UPDATE partnerhandlerschema.partners SET %I = $1 WHERE id = $2', column);
  pool.query(sql, [value, id], (err ,results) => {
    if (err) {
      throw err;
    }
    response.json({
      status: "success",
      count: results.rowCount,
      message: `Partner Details have been successfully updated`,
  })
})
}

const partnerEndpoint = (request, response, next) => {
    
    if (request.body.hasOwnProperty('partnerEndpointId')) {
      // Update Scenario
      updatePartnerEndpointInfo(request, response, next); 
    } else {
      // Insert Scenario
      insertPartnerEndpointInfo(request, response, next);
    }
}

const insertPartnerEndpointInfo = (request, response, next) => {
  try {
    let [
      sourceEndpointId,
      partnerId,
      displayName,
      url,
      createdOn,
      updatedOn
    ] = [
      utility.getObject(request, "body.sourceEndpointId"),
      utility.getObject(request, "body.partnerId"),
      utility.getObject(request, "body.displayName", ""),
      utility.getObject(request, "body.url", ""),
      utility.getObject(request, "body.createdOn", new Date(Date.now()).toISOString()),
      utility.getObject(request, "body.updatedOn", new Date(Date.now()).toISOString()),
    ];

    pool.query(
      "INSERT INTO partnerhandlerschema.partner_endpoints(endpoint_id, partner_id, display_name, url, created_on, updated_on) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id",
      [
        sourceEndpointId,
        partnerId,
        displayName,
        url,
        createdOn,
        updatedOn
      ],
      (error, results) => {
        if (error) {
          throw error;
        }

        response.json({
          status: "success",
          count: results.rowCount,
          id: results.rows[0].id,
          message: `Partner Endpoint has been added succesfully.`,
        });
      }
    );
  } catch(err) {
      next(err)
    }
}


const updatePartnerEndpointInfo = (request, response, next) => {
  try {
    let [
      partnerEndpointId,
      displayName,
      url,
      updatedOn
    ] = [
      utility.getObject(request, "body.partnerEndpointId"),
      utility.getObject(request, "body.displayName", ""),
      utility.getObject(request, "body.url", ""),
      utility.getObject(request, "body.updatedOn", new Date(Date.now()).toISOString()),
    ];

    pool.query(
      `UPDATE partnerhandlerschema.partner_endpoints 
      SET display_name = $1, url = $2, updated_on = $3
      WHERE id = $4 Returning display_name, url, id as partnerEndpointId`,
      [
        displayName,
        url,
        updatedOn,
        partnerEndpointId
      ], (err, results) => {
        console.log(err, results)
        if (err) {
          throw err;
        } else {
          response.json({ status: 'success', message: 'successfully updated partner endpoint information.', data: results.rows, count: results.rows.length})
        }
      })

  } catch(err) {
    next(err)
  }
}

const savePartnerMappings = (request, response, next) => {
  if (request.body.hasOwnProperty('partnerEndpointId')) {
    // Insert Mappings
    insertPartnerMappings(request, response, next);
  } else {
    // Update Mappings
    updatePartnerMappings(request, response, next);
  }  
}

const insertPartnerMappings = (request, response, next) => {
  try {
    const { partnerEndpointId, mappings } = request.body;
    var arr = [];
    var str = '';
    mappings.forEach(mapping => {
      var mappingDetails = {
        partner_endpoint_id: partnerEndpointId,
        key_id: mapping.keyId,
        mapping: mapping.mapping,
        created_on: new Date(Date.now()).toISOString(),
        updated_on: new Date(Date.now()).toISOString()
      }
      arr.push(mappingDetails);
    })
    var values = arr.map(a => {
      return `(${a.partner_endpoint_id}, ${a.key_id}, '${a.mapping}', '${a.created_on}', '${a.updated_on}')`;
    });
    var query = request.params.type === 'request' ?
    'INSERT INTO partnerhandlerschema.request_mappings(partner_endpoint_id, key_id, mapping, created_on, updated_on) VALUES '
    : 'INSERT INTO partnerhandlerschema.response_mappings(partner_endpoint_id, key_id, mapping_value, created_on, updated_on) VALUES '
    str = query + values.join(',');
    pool.query(str,
      (error, results) => {
        if (error) {
          throw error;
        }
        response.json({
          status: "success",
          count: results.rowCount,
          message: `Partner Mappings has been added succesfully.`,
        });
      })}
  catch (err) {
    next(err);
  }
}

const updatePartnerMappings = (request, response, next) => {
  try {
    let stringQuery = '';
    const { mappings } = request.body;
    console.log(request.body, request.body.mappings)
    const values = mappings.reduce((acc, curr, index) => acc + '(' + curr.mappingId + `,'` + curr.mapping +
    (index === mappings.length - 1 ?  `')`: `'),`), '')


    if (request.params.type === 'request') {
      stringQuery = `UPDATE partnerhandlerschema.request_mappings as t set
        mapping = c.mappingValue FROM (VALUES ${values})
        AS c(mappingId, mappingValue) WHERE c.mappingId  = t.id 
        RETURNING t.id as mappingId, t.mapping;`
    } else if(request.params.type === 'response') {
      stringQuery = `UPDATE partnerhandlerschema.response_mappings as t set
        mapping_value = c.mappingValue FROM (VALUES ${values})
        AS c(mappingId, mappingValue) WHERE c.mappingId  = t.id
        RETURNING t.id as mappingId, t.mapping_value;`
    }
    console.log(stringQuery);
    pool.query(stringQuery, (err, results) => {
      console.log(err, results)
      if (err) {
        throw err;
      } else {
        response.json({message: 'mappings updated succesfully', status: 'success', data: results.rows, count: results.rowCount.length})
      }
    })
  } catch(err) {
    next(err);
  }
}

const addJoltSchemaForPartnerEndpoint = (request, response, next) => {
  try {
    const values = [request.body.joltSchema, request.body.updatedOn, request.body.partnerEndpointId]
    
    let stringQuery = request.params.type === 'request' ?
    'UPDATE partnerhandlerschema.partner_endpoints SET request_jolt_schema= $1, updated_on= $2 WHERE id=$3':
    'UPDATE partnerhandlerschema.partner_endpoints SET response_jolt_schema= $1, updated_on=$2 WHERE id=$3'; 

    pool.query(stringQuery, values, (err ,results) => {
      if (err) {
        throw err;
      }
      response.json({
        status: "success",
        count: results.rowCount,
        message: `Jolt Schema for partner endpoint has been added succesfully.`,
    })
  })
}
  catch (err) {
    next(err);
  }
}
const signInUser = (request, response, next) => {
  try {
    const { email, password } = request.body;
    response.setHeader("Access-Control-Allow-Origin", "*");
    pool.query(
      "SELECT id, partner_id, user_email FROM partnerhandlerschema.users WHERE user_email = $1 and user_password = $2",
      [ email, md5(password) ], (err, res) => {
        if (err) {
          throw err;
        }
        else if (!res.rowCount) {
          response.status(404).json({ error: 'error', message: 'Please Check your username or password'});
        } else { 
        response.json({
          status: "success",
          count: 1,
          message: `User has been authenticated succesfully.`,
          data: [res.rows[0]]
        })
      }
    }
    )
  }
  catch(err) {
    throw err;
  }
}
  const registerUser = (request, response, next) => {
    try {
      const { email, password, createdOn } = request.body;
      response.setHeader("Access-Control-Allow-Origin", "*");
      pool.query(
        "SELECT id FROM partnerhandlerschema.users WHERE user_email = $1",
        [ email ], (err, res) => {
        if (err) {
          throw err;
        } else if (res.rowCount) {
          response.status(404).json({ error: 'error', message: 'User with same email already exists'});
        } else {

      pool.query(
        "INSERT INTO partnerhandlerschema.users(user_email, user_password, created_on) VALUES ($1, $2, $3) RETURNING id",
        [ email, md5(password), createdOn ], (err, res) => {
          if (err) {
            throw err;
          }
          response.json({
            status: "success",
            count: 1,
            message: `User has registered succesfully.`,
            id: res.rows[0].id
        })
        }
      )
    }
    })
    }
    catch(err) {
      throw err;
    }
  }

  module.exports = {
      getPartnerDetails,
      createPartner,
      getAvailableEndpoints,
      getPartnerMappings,
      partnerEndpoint,
      savePartnerMappings,
      getKeysbyEndpoint,
      addJoltSchemaForPartnerEndpoint,
      updatePartnerDetails,
      signInUser,
      registerUser,
      getPartnerEndpointInfo
  }
  