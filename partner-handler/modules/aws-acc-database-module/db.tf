# variable "subnet_name"{}
 variable "sg_id" {}

# variable "sg_id" {
#   type = "list"
#   description = "security group name"
# }
# module "network" {
#   source = "./vpc"
#   sg_id = ["${module.vpc.sg_id}"]
# }
resource "aws_db_instance" "terraPostgres" {
  allocated_storage = 20
  storage_type = "gp2"
  engine = "postgres"
  engine_version = "12.4"
  instance_class = "db.t2.micro"
  name = var.db_name #"test10011" #var.databaseName
  username = var.db_username  # "postgres"
  password = var.db_password #"postgres"
  skip_final_snapshot = false
  deletion_protection = false
 db_subnet_group_name = aws_db_subnet_group.postgresql_subnet_group.name
  vpc_security_group_ids = [var.sg_id]
}

resource "aws_db_subnet_group" "postgresql_subnet_group" {
    name       = "postgresubgroup"
    subnet_ids = [var.subnet_id,var.subnet_id2]

    tags = {
        Name = "PostgreSQL subnet group"
    }
}
output "db_name" {
    value = aws_db_instance.terraPostgres.name
  }
output "db_username" {
   value = aws_db_instance.terraPostgres.username
  }
output "db_password" {
    value =  aws_db_instance.terraPostgres.password
}
output "db_port" {
    value = aws_db_instance.terraPostgres.port
}

output "db_hostname" {
  value = aws_db_instance.terraPostgres.address
}

variable "db_hostname"{}
variable "db_name" {}
variable "db_username" {}
variable "db_password" {}
variable "db_port" {}
variable "subnet_id"{}
variable "subnet_id2"{}