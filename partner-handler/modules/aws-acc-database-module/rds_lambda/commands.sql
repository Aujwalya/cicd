
CREATE SCHEMA partnerhandlerschema;
CREATE TABLE partnerhandlerschema.partners(
	id serial,
	name varchar(100),
	category varchar(50),
	cuisine varchar(50),
	average_cost int,
	rating numeric,
	location varchar(100),
	operation_timings varchar(100),
	is_active boolean,
	address text,
	created_on timestamp,
	updated_on timestamp,
	image_url text,
	PRIMARY KEY(id)
);

CREATE TABLE partnerhandlerschema.users(
	id serial,
	partner_id integer,
	user_email text,
	user_password text,
	created_on timestamp,
	updated_on timestamp,
	PRIMARY KEY(id),
	CONSTRAINT fkey_partner_user_id
		FOREIGN KEY(partner_id)
			REFERENCES partnerhandlerschema.partners(id)

);

CREATE TABLE partnerhandlerschema.source_endpoints(
	id serial,
	name varchar(100),
	slug varchar(50),
	endpoint text,
	request jsonb,
	response jsonb,
	header jsonb,
	created_on timestamp,
	updated_on timestamp,
	PRIMARY KEY(id)
);

CREATE TABLE partnerhandlerschema.partner_endpoints(
	id serial,
	endpoint_id int,
	partner_id int,
	display_name varchar(100),
	url text,
	request_jolt_schema jsonb,
	response_jolt_schema jsonb,
	created_on timestamp,
	updated_on timestamp,
	PRIMARY KEY(id),
	CONSTRAINT fkey_partner_id
			FOREIGN KEY(partner_id)
				REFERENCES partnerhandlerschema.partners(id),
	CONSTRAINT fkey_endpoint_id
		FOREIGN KEY(endpoint_id)
			REFERENCES partnerhandlerschema.source_endpoints(id)

);

CREATE TABLE partnerhandlerschema.source_endpoint_request_keys(
	id serial,
	source_endpoint_id int,
	key_name varchar(100),
	created_on timestamp,
	updated_on timestamp,
	PRIMARY KEY(id),
	CONSTRAINT source_endpoint_request_fkey
		FOREIGN KEY(source_endpoint_id) 
			REFERENCES partnerhandlerschema.source_endpoints(id)	

);

CREATE TABLE partnerhandlerschema.source_endpoint_response_keys(
	id serial,
	source_endpoint_id int,
	key_name varchar(100),
	created_on timestamp,
	updated_on timestamp,
	PRIMARY KEY(id),
	CONSTRAINT source_endpoint_response_fkey
		FOREIGN KEY(source_endpoint_id) 
			REFERENCES partnerhandlerschema.source_endpoints(id)	

);

CREATE TABLE partnerhandlerschema.request_mappings(
	id serial,
	partner_endpoint_id int,
	key_id int,
	mapping text,
	created_on timestamp,
	updated_on timestamp,
	PRIMARY KEY(id),
	CONSTRAINT fkey_endpoint_id_request
		FOREIGN KEY(partner_endpoint_id)
			REFERENCES partnerhandlerschema.partner_endpoints(id),
	CONSTRAINT fkey_key_id_request
		FOREIGN KEY(key_id)
			REFERENCES partnerhandlerschema.source_endpoint_request_keys(id)
);

CREATE TABLE partnerhandlerschema.response_mappings(
	id serial,
	partner_endpoint_id int,
	key_id int,
	mapping_value text,
	created_on timestamp,
	updated_on timestamp,
	PRIMARY KEY(id),
	CONSTRAINT fkey_endpoint_id_response
		FOREIGN KEY(partner_endpoint_id)
			REFERENCES partnerhandlerschema.partner_endpoints(id),
	CONSTRAINT fkey_key_id_response
		FOREIGN KEY(key_id)
			REFERENCES partnerhandlerschema.source_endpoint_response_keys(id)
);

CREATE TABLE partnerhandlerschema.transactions(
	id serial,
	partner_id int,
	endpoint_id int,
    date_time timestamp,
	status varchar(50),
	log_message varchar(50),
	created_on timestamp,
	updated_on timestamp,
	PRIMARY KEY(id),
	CONSTRAINT fkey_partner_id
		FOREIGN KEY(partner_id)
			REFERENCES partnerhandlerschema.partners(id),
	CONSTRAINT fkey_endpoint_id
		FOREIGN KEY(endpoint_id)
			REFERENCES partnerhandlerschema.source_endpoints(id)
);

CREATE TABLE partnerhandlerschema.orders
(
    orderid character varying(255) COLLATE pg_catalog."default" NOT NULL,
    amountpaid integer,
    createdon timestamp without time zone,
    currentstatus character varying(255) COLLATE pg_catalog."default",
    estimatedtimemin integer,
    isdelivered boolean,
    item_details jsonb,
    lastmodified timestamp without time zone,
    partnerid integer,
    paymentid integer
);

INSERT INTO partnerhandlerschema.partners(
	name, category, cuisine, average_cost, rating, location, operation_timings, is_active, image_url, address, created_on, updated_on)
	VALUES ('Behrouz Biryani', 'Delivery', 'Biryani, Spicy', 50, 5, 'Delhi', '09:00 21:00', true, 'https://images.unsplash.com/photo-1552590635-27c2c2128abf?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80', 'Connaught Place', '2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');

INSERT INTO partnerhandlerschema.source_endpoints(
	name, endpoint, response, header, created_on, updated_on)
	VALUES ('GetMenu', 'https://myAwsEC2Url/api/v1/partner/getPartnerDetails', '{"success": true, "count": 0, "data": [],
			"dishId": "${dishId}",
            "dishName": "${dishName}",
            "dishCategory": "${dishCategory}",
            "dishDescription": "${dishDescription}",
            "dishRecommended": "${dishRecommended}",
            "dishInStock": "${dishInStock}",
            "dishIsVeg": "${dishIsVeg}",
            "dishPrice": "${dishPrice}",
            "dishEnabled": "${dishEnabled}"
			}',
	'{"Accept": "application/json"}', '2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	
  
INSERT INTO partnerhandlerschema.source_endpoints(
	name, endpoint, request, response, header, created_on, updated_on)
	VALUES ('SubmitOrder', 'https://myAwsEC2Url/api/v1/order/submitOrder',
	'{"partnerId": "${partnerId}",
	"item_details": [{
		"itemId": "${itemId}",
		"type": "${type}",
		"cost": "${cost}",
		"count": "${count}"
	}]}',

	'{"status": "${status}",
    "message": "${message}",
    "count": "${count}",
    "data": [{"orderId": "${orderId}",
       "currentStatus": "${currentStatus}",
       "partnerId": "${partnerId}",
       "lastModified": "${lastModified}",
       "isDelivered": "${isDelivered}",
       "estimatedTimeMin": "${estimatedTimeMin}"}]

			}',
	'{"Accept": "application/json"}', '2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');

			
INSERT INTO partnerhandlerschema.source_endpoint_response_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (1, 'dishId','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	INSERT INTO partnerhandlerschema.source_endpoint_response_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (1, 'dishName','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	INSERT INTO partnerhandlerschema.source_endpoint_response_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (1, 'dishCategory','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	INSERT INTO partnerhandlerschema.source_endpoint_response_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (1, 'dishDescription','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	INSERT INTO partnerhandlerschema.source_endpoint_response_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (1, 'dishRecommended','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	INSERT INTO partnerhandlerschema.source_endpoint_response_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (1, 'dishInStock','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	INSERT INTO partnerhandlerschema.source_endpoint_response_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (1, 'dishIsVeg','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
		INSERT INTO partnerhandlerschema.source_endpoint_response_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (1, 'dishPrice','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
			INSERT INTO partnerhandlerschema.source_endpoint_response_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (1, 'dishEnabled','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	


INSERT INTO partnerhandlerschema.source_endpoint_request_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (2, 'partnerId','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	INSERT INTO partnerhandlerschema.source_endpoint_request_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (2, 'itemId','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	INSERT INTO partnerhandlerschema.source_endpoint_request_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (2, 'type','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	INSERT INTO partnerhandlerschema.source_endpoint_request_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (2, 'cost','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	INSERT INTO partnerhandlerschema.source_endpoint_request_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (2, 'count','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');



INSERT INTO partnerhandlerschema.source_endpoint_response_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (2, 'currentStatus','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	INSERT INTO partnerhandlerschema.source_endpoint_response_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (2, 'orderId','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	INSERT INTO partnerhandlerschema.source_endpoint_response_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (2, 'partnerId','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	INSERT INTO partnerhandlerschema.source_endpoint_response_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (2, 'lastModified','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	INSERT INTO partnerhandlerschema.source_endpoint_response_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (2, 'isDelivered','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');
	INSERT INTO partnerhandlerschema.source_endpoint_response_keys(
	source_endpoint_id, key_name,  created_on, updated_on)
	VALUES (2, 'estimatedTimeMin','2020-10-22 08:25:20.231', '2020-10-22 08:25:20.231');

CREATE TABLE partnerhandlerschema.partner_mockdata(
id int,
mock_data jsonb);

CREATE TABLE partnerhandlerschema.menu_mockdata(
id int,
mock_data jsonb);

INSERT INTO partnerhandlerschema.menu_mockdata(id, mock_data)
VALUES (1, '{
"id": 1,
"name": "Dominos",
"address":"MNO city",
"costForTwoMsg": 500,
"deliveryTime": 30,
"menu": [
{
"id": 101,
"name": "Farmhouse Pizza",
"description": "Farmhouse Pizza",
"rating": 9,
"imgUrl": "https://images.pexels.com/photos/3928854/pexels-photo-3928854.png?auto=compress&cs=tinysrgb&dpr=2&w=500",
"isAvailable": true,
"veg": true,
"qty": 1,
"dishEnabled": true,
"size": [
"small",
"medium",
"large"
],
"price": 440
},
{
"id": 102,
"name": "Peppy Paneer Pizza",
"description": "Peppy Paneer Pizza",
"rating": 9,
"imgUrl": "https://images.pexels.com/photos/2580464/pexels-photo-2580464.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500",
"isAvailable": true,
"veg": false,
"qty": 1,
"dishEnabled": true,
"size": [
"small",
"medium",
"large"
],
"price": 520
}
]
}');

INSERT INTO partnerhandlerschema.menu_mockdata(id, mock_data)
VALUES (2, '{
"id": 2,
"name": "Behrouz Biryani",
"address":
{
"title": "ABC Street",
"description": "Landmark"
},
"cuisines":{
"items": [
{
"id": 101,
"name": "Paneer Biryani",
"rating": 4.2,
"in_stock": true,
"is_veg": true
},
{
"id": 102,
"name": "Chicken Biryani",
"rating": 4.7,
"in_stock": true,
"is_veg": true
}
]
}
}');

INSERT INTO partnerhandlerschema.menu_mockdata(id, mock_data)
VALUES (3, '{
"menulist": [
{
"id": 101,
"qty": 1,
"name": "Chocolate Sundae",
"price": 370,
"imgUrl": "https://www.google.com/imgres?imgurl=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fd%2Fd8%2FBaskin-Robbins_logo.svg%2F1200px-Baskin-Robbins_logo.svg.png&imgrefurl=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FBaskin-Robbins&tbnid=mZBXL-n0LWwLJM&vet=12ahUKEwipspXlnJ3tAhWqkksFHXYsBVgQMygAegUIARDUAQ..i&docid=6oWsOlushsKa0M&w=1200&h=1200&q=baskin%20robbins%20image&ved=2ahUKEwipspXlnJ3tAhWqkksFHXYsBVgQMygAegUIARDUAQ",
"rating": 9,
"description": "Loaded with choco chips and caramel",
"isAvailable": true
},
{
"id": 102,
"qty": 1,
"name": "Fruit Ice Cream",
"price": 250,
"imgUrl": "https://www.google.com/imgres?imgurl=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fd%2Fd8%2FBaskin-Robbins_logo.svg%2F1200px-Baskin-Robbins_logo.svg.png&imgrefurl=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FBaskin-Robbins&tbnid=mZBXL-n0LWwLJM&vet=12ahUKEwipspXlnJ3tAhWqkksFHXYsBVgQMygAegUIARDUAQ..i&docid=6oWsOlushsKa0M&w=1200&h=1200&q=baskin%20robbins%20image&ved=2ahUKEwipspXlnJ3tAhWqkksFHXYsBVgQMygAegUIARDUAQ",
"rating": 8,
"description": "A delicious fruit ice cream",
"isAvailable": true
},
{
"id": 103,
"qty": 1,
"veg": true,
"name": "Strawberry Cup",
"price": 250,
"imgUrl": "https://www.google.com/imgres?imgurl=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fd%2Fd8%2FBaskin-Robbins_logo.svg%2F1200px-Baskin-Robbins_logo.svg.png&imgrefurl=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FBaskin-Robbins&tbnid=mZBXL-n0LWwLJM&vet=12ahUKEwipspXlnJ3tAhWqkksFHXYsBVgQMygAegUIARDUAQ..i&docid=6oWsOlushsKa0M&w=1200&h=1200&q=baskin%20robbins%20image&ved=2ahUKEwipspXlnJ3tAhWqkksFHXYsBVgQMygAegUIARDUAQ",
"rating": 9,
"description": "A delicious fruit ice cream",
"isAvailable": true
}
],
"name": "Baskin Robbins",
"address": "DLF CyberHub",
"deliveryTime": 30,
"costForTwoMsg": 500
}');

INSERT INTO partnerhandlerschema.partner_mockdata(id, mock_data)
VALUES (1, '{
"res": {
"eta": "30",
"payId": "1234",
"amount": "750",
"orderId": "ORD001",
"createdOn": "Thu Nov 05 2020 12:33:10 GMT+0530 (India Standard Time)",
"isDelivered": "false",
"currentStatus": "Preparing"
},
"count": "1",
"status": "Processed",
"message": "123"
}');

INSERT INTO partnerhandlerschema.partner_mockdata(id, mock_data)
VALUES (2, '{
"count": "1",
"status": "Processed",
"message": "123",
"orderDetails": [
{
"id": "ORD110",
"paymenId": "23456",
"createdOn": "Thu Nov 05 2020 12:33:10 GMT+0530 (India Standard Time)",
"amountToPay": "390",
"orderStatus": "Preparing",
"deliveryTime": "20",
"deliveryStatus": "false"
}
]
}');