import sys
import logging
#import config
#import pymysql
import psycopg2
import os
#rds settings
rds_host  =os.environ.get('rds_host').partition(':')[0] #"database-1.c3579beqmzmb.us-east-1.rds.amazonaws.com"
name = region = os.environ.get('name')
password = region = os.environ.get('password')
db_name =region = os.environ.get('db_name')
logger = logging.getLogger()
logger.setLevel(logging.INFO)
try:
    conn = psycopg2.connect(database=db_name, user = name, password = password, host = rds_host, port = "5432")

except:
    print("ERROR: Unexpected error: Could not connect to Postgress RDS instance.")
    #sys.exit()
logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
def lambda_handler(event, context):
    """
    This function fetches content from mysql RDS instance
    """
    item_count = 0
    cursor = conn.cursor()
    sql_file = open("commands.sql")
    sql_as_string = sql_file.read()
    cursor.execute(sql_as_string)
    cursor.close()
    # commit the changes
    conn.commit()