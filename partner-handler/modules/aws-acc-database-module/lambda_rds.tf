
resource "aws_lambda_function" "rds_lambda" {
  function_name = "rds_lambda"
  filename      = "../modules/aws-acc-database-module/rds_lambda1.zip"
  #filename         = data.archive_file.first_zip.output_path
  #source_code_hash = data.archive_file.first_zip.output_base64sha256
  role = aws_iam_role.rds_vpc.arn
  handler = "postgress.lambda_handler"
  runtime = "python3.8"
  timeout = 10
  vpc_config {
    subnet_ids         = [var.subnet_id,var.subnet_id2]
    security_group_ids = [var.sg_id]
  }
  environment {
    variables = {
      rds_host  = aws_db_instance.terraPostgres.endpoint
      name = aws_db_instance.terraPostgres.username
      password = aws_db_instance.terraPostgres.password
      db_name = aws_db_instance.terraPostgres.name
    
  }
  }
  layers = [ aws_lambda_layer_version.lambda_layer.arn ]
}
resource "aws_lambda_layer_version" "lambda_layer" {
  layer_name = "SQlPostgressLib_Module"
  filename = "../modules/aws-acc-database-module/SQlPostgressLib.zip"
  compatible_runtimes = ["python3.8","python3.7","python3.6"]
  
}
resource "aws_iam_role" "rds_vpc" {
   name = "rds_vpc"
 
   assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["lambda.amazonaws.com","lex.amazonaws.com"]
      },
      "Effect": "Allow",
      "Sid": ""
      
    }
  ]
}
EOF
 
}

resource "aws_iam_role_policy" "frontend_lambda_role_policy" {
  name   = "frontend-lambda-role-policy"
  role   = "${aws_iam_role.rds_vpc.id}"
  policy = file("../modules/aws-acc-database-module/rds_lambda_policy.json")
}

# data "aws_lambda_invocation" "example" {
#   function_name = aws_lambda_function.rds_lambda.function_name

#   input = <<JSON
# {
#   "key1": "value1",
#   "key2": "value2"
# }
# JSON
# }