variable "Bucket_name"{}
variable "endpoint"{}
resource "aws_cloudfront_distribution" "main" {
  http_version = "http2"

  origin {
    origin_id   = "origin-${var.Bucket_name}"
    domain_name = var.endpoint

    custom_origin_config {
      origin_protocol_policy = "http-only"
      http_port              = "80"
      https_port             = "443"
      origin_ssl_protocols   = ["TLSv1.2"]
    }

    custom_header {
      name  = "User-Agent"
      value = "SECRET-STRING"
    }
  }

  enabled             = true
  default_root_object = "index.html"

  #aliases = concat([local.Bucket_name_name])

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  default_cache_behavior {
    target_origin_id = "origin-${var.Bucket_name}"
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    compress         = true

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 300
    max_ttl                = 1200
  }

  viewer_certificate {
   cloudfront_default_certificate = true
  }
}