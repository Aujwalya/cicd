output "ecr-module"{
    value="${module.my_ecr.ecr_output}"
}

output "db_hostname"{
    value= {
    db_hostname= "${module.my_db.db_hostname}",
    db_username="${module.my_db.db_username}",
    db_password="${module.my_db.db_password}",
    db_port="${module.my_db.db_port}",
    }
}

output "json_path-Finder-Url"{
    value="${module.json-path-finder.json_path-Finder-Url}"
}