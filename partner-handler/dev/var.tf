variable "ecr_name" {
  description = "variable for ECR module."
type = string
}

variable "ecr_image_tag_mutability" {
  description = "value to tag mutability (IMMUTABLE or MUTABLE)"
  type = string
  default = "IMMUTABLE"
}




variable  "db_name"{
    description = "Db Name"
    type = string
}
variable "db_username" {
    description = "Username"
    type = string
}
variable "db_password"{
    description = "Password"
    type = string

}