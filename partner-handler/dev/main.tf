provider "aws" {
    region="us-west-1"
    access_key = "AKIA3HS75MQ7CUYNV7UF"
    secret_key = "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"
}

module "acc-aws-documentDb"{
    source="../modules/acc-aws-documentDb"
    subnet_id = "${module.my_vpc.subnet_id}"
    aws_instance = "${module.my_ec2.aws_instance}"
    sg_id="${module.my_vpc.sg_id}"
    subnet_id2 = "${module.my_vpc.subnet_id2}"
}

module "my_vpc" {
    source = "../modules/acc-aws-vpc"
    vpc_cidr = "192.168.0.0/16"
    # tenancy = "default"
    vpc_id = "${module.my_vpc.vpc_id}"
    subnet_cidr = "192.168.3.0/24"
    subnet_cidr1 = "192.168.2.0/24"
    sg = "${module.my_vpc.sg}"
    # subnet_name ="${module.my_vpc.subnet_name}"
   sg_id="${module.my_vpc.sg_id}"
}

module "my_ec2" {
    source = "../modules/acc-aws-ec2"
    subnet_id = "${module.my_vpc.subnet_id}"
    sg_id="${module.my_vpc.sg_id}"
}

module "my_ecr" {
   ecr_name ="${var.ecr_name}"
   ecr_image_tag_mutability = "${var.ecr_image_tag_mutability}"
   source = "../modules/acc-aws-ecr-module"
}

module "my_db" {
   source = "../modules/aws-acc-database-module"
   subnet_id = "${module.my_vpc.subnet_id}"
   sg_id="${module.my_vpc.sg_id}"
   subnet_id2 = "${module.my_vpc.subnet_id2}"
   db_hostname="${module.my_db.db_hostname}"
   db_name="${var.db_name}"
   db_username="${var.db_username}"
   db_password="${var.db_password}"
   db_port="${module.my_db.db_port}"
 }

 module "acc_aws_lex"{
    source="../modules/acc-aws-lex"
 }

 module "serverless-terraform-on-boarding"{
     # depends_on = [
     #   module.my_db
     # ]
     source ="../modules/acc-aws-serverless/terraform"
     sg_id="${module.my_vpc.sg_id}"
     subnet_id = "${module.my_vpc.subnet_id}"
     subnet_id2 = "${module.my_vpc.subnet_id2}"
     db_hostname="${module.my_db.db_hostname}"
     db_name="${module.my_db.db_name}"
     db_username="${module.my_db.db_username}"
     db_password="${module.my_db.db_password}"
     db_port="${module.my_db.db_port}"
     #  ami_id= "${module.my_ec2.ami_id}"
     #  instance_type = "t2.micro"
      aws_instance = "${module.my_ec2.aws_instance}"
 }

module "serverless-jwt" {
   source ="../modules/acc-aws-jwt-serverless/terraform"
     aws_instance = "${module.my_ec2.aws_instance}"
}

module "s3" {
  source = "../modules/acc-aws-s3-jsonPathFinder"
}

module "json-path-finder" {
  source = "../modules/acc-aws-cloudFront-jsonPathFinder"
  Bucket_name=module.s3.Bucket_name
  endpoint=module.s3.endpoint
}